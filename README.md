# sepa-broker

### Build and run
```bash
./gradlew clean build
java -jar build/libs/sepa-broker.jar
```

### Generate test metrics
```bash
./gradlew clean pitest
```

### Sonar
```bash
./gradlew clean build sonarqube -Dsonar.host.url=http://localhost:9000
```

### Local deployment
```bash

# Set Docker environment
eval $(minikube docker-env)

# Create config map
kubectl create configmap sepa-broker-apidoc --from-file=./src/testIntegration/resources/apidoc.yaml

# Build
./gradlew clean build

# Docker build
docker build -t local/sepa-broker:$(git rev-parse --short HEAD) .

# Helm install
helm upgrade --install --force sepa-broker ./kube \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=local/sepa-broker:$(git rev-parse --short HEAD)

# All together
./gradlew clean build && \
docker build -t local/sepa-broker:$(git rev-parse --short HEAD) . && \
helm upgrade --install --force sepa-broker ./kube \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=local/sepa-broker:$(git rev-parse --short HEAD)


```

## Local environment

```
docker-compose up
```


## File performance tests
|Scenario                              |Slapsed time to generate |Elapsed time to send|  
|--------------------------------------|-------------------------|--------------------|
|50k payments (-Xmx3072m), 81M file    |15637 millis             |4458 millis         |
|100k payments (-Xmx3072m), 162M file  |30110 millis             |5183 millis         |
|100k payments (-Xmx512m), 162M file   |55778 millis             |5260 millis         |

