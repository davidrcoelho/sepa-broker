package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPaymentRepository;
import eu.gentleart.sepa.broker.pain.PaymentListSummarizer;
import eu.gentleart.sepa.broker.pain.PaymentListSummary;
import eu.gentleart.sepa.broker.pain.XmlMarshaller;
import iso.std.iso._20022.tech.xsd.pain_008_001.CustomerDirectDebitInitiationV02;
import iso.std.iso._20022.tech.xsd.pain_008_001.Document;
import iso.std.iso._20022.tech.xsd.pain_008_001.GroupHeader39;
import iso.std.iso._20022.tech.xsd.pain_008_001.PaymentInstructionInformation4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.io.OutputStream;

public class Pain08XmlDocumentBuilder {

    private static final Logger logger = LoggerFactory.getLogger(Pain08XmlDocumentBuilder.class);

    private DirectDebitPaymentRepository paymentRepository;
    private DirectDebitDocument directDebitDocument;

    public Pain08XmlDocumentBuilder withPaymentRepository(DirectDebitPaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
        return this;
    }

    public Pain08XmlDocumentBuilder withDirectDebitDocument(DirectDebitDocument directDebitDocument) {
        this.directDebitDocument = directDebitDocument;
        return this;
    }

    public void build(OutputStream outputStream) throws DatatypeConfigurationException, JAXBException, IOException {

        logger.info("Creating summary to generate file.");

        PaymentListSummarizer summarizer = new PaymentListSummarizer(paymentRepository, directDebitDocument);

        GroupHeader39 header = new Pain08HeaderBuilder()
                .withDocument(directDebitDocument)
                .withTotalNumberOfTransactions(summarizer.getTotalNumberOfTransactions())
                .withTotalPaymentSum(summarizer.getTotalPaymentSum())
                .build();

        CustomerDirectDebitInitiationV02 customerDirectDebitInitiationv02 = new CustomerDirectDebitInitiationV02();
        customerDirectDebitInitiationv02.setGrpHdr(header);

        for (PaymentListSummary summary : summarizer.listSummary()) {
            logger.info("Adding payments to the file. sequenceType: {}", summary.getSequenceType());
            PaymentInstructionInformation4 paymentInstructionInformation = new Pain08PaymentBuilder()
                    .withDocument(directDebitDocument)
                    .withSummary(summary)
                    .build();
            customerDirectDebitInitiationv02.getPmtInf().add(paymentInstructionInformation);
            logger.info("Payments added to the file. sequenceType: {}", summary.getSequenceType());
        }

        Document xmlDocument = new Document();
        xmlDocument.setCstmrDrctDbtInitn(customerDirectDebitInitiationv02);

        XmlMarshaller marshaller = new XmlMarshaller();
        marshaller.marshall(xmlDocument, outputStream);
    }

}
