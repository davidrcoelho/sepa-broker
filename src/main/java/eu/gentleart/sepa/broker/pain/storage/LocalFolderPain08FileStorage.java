package eu.gentleart.sepa.broker.pain.storage;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Component
public class LocalFolderPain08FileStorage implements Pain08FileStorage {

    private static final Logger logger = LoggerFactory.getLogger(LocalFolderPain08FileStorage.class);

    private static final String PAIN08_PREFIX = "sepa-pain08_";

    private static final Date SIX_HOUR_AGO_CUTOFF_DATE = Date.from(
            LocalDateTime.now()
                    .minus(6, ChronoUnit.HOURS)
                    .atZone(ZoneId.systemDefault())
                    .toInstant());

    @Value("${sepa.appFolder}")
    private String appFolder;

    @Override
    public FileOutputStream getFileOutputStream(String filename) throws FileNotFoundException {
        String xmlFilename = getFullPath(filename);
        return new FileOutputStream(new File(xmlFilename));
    }

    private String getFullPath(String filename) {
        return appFolder + "/" + filename;
    }

    @Override
    public FileInputStream getFileInputStream(String filename) throws FileNotFoundException {
        String xmlFilename = getFullPath(filename);
        return new FileInputStream(new File(xmlFilename));
    }

    @Override
    public String getPain08Filename(DirectDebitDocument document) {
        return PAIN08_PREFIX + document.getId() + ".xml";
    }

    @Override
    public FileInputStream createMD5File(String filename, String md5Filename) throws IOException {
        String filePath = getFullPath(filename);
        String md5FilePath = getFullPath(md5Filename);

        logger.info("Creating md5 for file: {}", filePath);

        String md5 = generateMD5(filePath);

        File md5File = new File(md5FilePath);
        FileUtils.writeStringToFile(md5File, md5, Charset.defaultCharset());
        return new FileInputStream(md5File);
    }

    private String generateMD5(String filename) throws IOException {
        return DigestUtils.md5DigestAsHex(FileUtils.openInputStream(new File(filename)));
    }

    @Override
    public boolean pain08FileExists(DirectDebitDocument document) {
        String filename = getPain08Filename(document);
        String filepath = getFullPath(filename);
        File file = new File(filepath);
        return (file.exists() && file.isFile());
    }

    @Override
    public String getMD5Filename(String pain08Filename) {
        return pain08Filename + ".md5";
    }

    @Override
    public void delete(String filename) {
        String filePath = getFullPath(filename);
        logger.info("Deleting file: {}", filePath);
        FileUtils.deleteQuietly(new File(filePath));
    }

    @Override
    public void removeOldFiles() {
        File[] files = listOldFiles();

        if (files.length == 0) {
            logger.info("No old files to be deleted.");

        } else {
            for (File file : files) {
                logger.info("Deleting old file: {}", file.getAbsolutePath());
                file.deleteOnExit();
            }
        }
    }

    private File[] listOldFiles() {

        IOFileFilter ageAndPrefixFilter = FileFilterUtils.and(
                new AgeFileFilter(SIX_HOUR_AGO_CUTOFF_DATE),
                new PrefixFileFilter(PAIN08_PREFIX)
        );

        return FileFilterUtils.filter(ageAndPrefixFilter, new File(appFolder));
    }

}
