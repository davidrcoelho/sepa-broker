package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.directdebit.document.*;
import eu.gentleart.sepa.broker.pain.PaymentListSummary;
import iso.std.iso._20022.tech.xsd.pain_008_001.*;

import javax.xml.datatype.DatatypeConfigurationException;

class Pain08PaymentBuilder {

    private DirectDebitDocument document;
    private PaymentListSummary summary;

    Pain08PaymentBuilder withDocument(DirectDebitDocument document) {
        this.document = document;
        return this;
    }

    Pain08PaymentBuilder withSummary(PaymentListSummary summary) {
        this.summary = summary;
        return this;
    }

    PaymentInstructionInformation4 build() throws DatatypeConfigurationException {

        DocumentHeader header = document.getDocumentHeader();
        PaymentHeader paymentHeader = document.getPaymentHeader();
        Creditor creditor = paymentHeader.getCreditor();
        CreditorAccount creditorAccount = creditor.getAccount();

        PaymentTypeInformation20 paymentTypeInformation = new Pain08PaymentTypeBuilder()
                .withSequenceType(summary.getSequenceType())
                .build();

        CashAccount16 cashAccount = new Pain08CashAccountBuilder()
                .withIban(creditorAccount.getIban())
                .build();

        BranchAndFinancialInstitutionIdentification4 branchAndFinancialInstitutionIdentification = new Pain08InstitutionIdentificationBuilder()
                .withBic(creditorAccount.getBic())
                .build();

        PartyIdentification32 partyIdentification = new Pain08PartyIdentificationBuilder()
                .withName(creditor.getName())
                .build();

        PaymentInstructionInformation4 paymentInstructionInfo = new PaymentInstructionInformation4();
        paymentInstructionInfo.setPmtMtd(PaymentMethod2Code.fromValue(paymentHeader.getPaymentMethod()));
        paymentInstructionInfo.setNbOfTxs(String.valueOf(summary.size()));
        paymentInstructionInfo.setCtrlSum(summary.getSum());
        paymentInstructionInfo.setPmtTpInf(paymentTypeInformation);
        paymentInstructionInfo.setCdtrAcct(cashAccount);
        paymentInstructionInfo.setCdtrAgt(branchAndFinancialInstitutionIdentification);
        paymentInstructionInfo.setPmtInfId(paymentHeader.getId());
        paymentInstructionInfo.setReqdColltnDt(header.getCreditorDateAsGregorianCalendar());
        paymentInstructionInfo.setCdtr(partyIdentification);

        summary.getPayments().forEach(directDebitPayment -> {
            DirectDebitTransactionInformation9 directDebitTransactionInformation = new Pain08TransactionBuilder()
                    .withPayment(directDebitPayment)
                    .build();
            paymentInstructionInfo.getDrctDbtTxInf().add(directDebitTransactionInformation);
        });


        return paymentInstructionInfo;
    }

}
