package eu.gentleart.sepa.broker.pain.pain08;

import iso.std.iso._20022.tech.xsd.pain_008_001.AccountIdentification4Choice;
import iso.std.iso._20022.tech.xsd.pain_008_001.CashAccount16;

class Pain08CashAccountBuilder {

    private String iban;

    Pain08CashAccountBuilder withIban(String iban) {
        this.iban = iban;
        return this;
    }

    CashAccount16 build() {
        AccountIdentification4Choice accountIdentification = new AccountIdentification4Choice();
        accountIdentification.setIBAN(iban);

        CashAccount16 result = new CashAccount16();
        result.setId(accountIdentification);
        return result;
    }
}
