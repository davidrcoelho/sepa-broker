package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.directdebit.payment.PaymentIdentification;
import iso.std.iso._20022.tech.xsd.pain_008_001.PaymentIdentification1;

class Pain08PaymentIdentificationBuilder {

    private PaymentIdentification identification;

    public Pain08PaymentIdentificationBuilder withPaymentIdentification(PaymentIdentification identification) {
        this.identification = identification;
        return this;
    }

    PaymentIdentification1 build() {
        PaymentIdentification1 paymentIdentification1 = new PaymentIdentification1();
        paymentIdentification1.setEndToEndId(identification.getEndToEndId());
        return paymentIdentification1;
    }
}
