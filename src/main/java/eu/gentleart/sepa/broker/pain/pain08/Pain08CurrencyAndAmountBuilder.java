package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.directdebit.payment.InstructedAmount;
import iso.std.iso._20022.tech.xsd.pain_008_001.ActiveOrHistoricCurrencyAndAmount;

class Pain08CurrencyAndAmountBuilder {

    private InstructedAmount instructedAmount;

    Pain08CurrencyAndAmountBuilder withInstructedAmount(InstructedAmount instructedAmount) {
        this.instructedAmount = instructedAmount;
        return this;
    }

    ActiveOrHistoricCurrencyAndAmount build() {
        ActiveOrHistoricCurrencyAndAmount currencyAndAmount = new ActiveOrHistoricCurrencyAndAmount();
        currencyAndAmount.setCcy(instructedAmount.getCurrency());
        currencyAndAmount.setValue(instructedAmount.getValue());
        return currencyAndAmount;
    }
}
