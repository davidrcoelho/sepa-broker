package eu.gentleart.sepa.broker.pain.pain08;

import iso.std.iso._20022.tech.xsd.pain_008_001.BranchAndFinancialInstitutionIdentification4;
import iso.std.iso._20022.tech.xsd.pain_008_001.FinancialInstitutionIdentification7;

class Pain08InstitutionIdentificationBuilder {

    private String bic;

    Pain08InstitutionIdentificationBuilder withBic(String bic) {
        this.bic = bic;
        return this;
    }

    BranchAndFinancialInstitutionIdentification4 build() {
        BranchAndFinancialInstitutionIdentification4 result = new BranchAndFinancialInstitutionIdentification4();
        result.setFinInstnId(new FinancialInstitutionIdentification7());
        result.getFinInstnId().setBIC(bic);
        return result;
    }


}
