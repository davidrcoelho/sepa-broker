package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.directdebit.payment.*;
import iso.std.iso._20022.tech.xsd.pain_008_001.*;

import javax.xml.datatype.DatatypeConfigurationException;

class Pain08MandateBuilder {

    private DirectDebitPayment payment;

    Pain08MandateBuilder withPayment(DirectDebitPayment payment) {
        this.payment = payment;
        return this;
    }

    DirectDebitTransaction6 build() throws DatatypeConfigurationException {

        DirectDebitTransaction6 directDebitTransaction = new DirectDebitTransaction6();

        Mandate mandate = payment.getMandate();

        MandateRelatedInformation6 mandateRelatedInformation6 = new MandateRelatedInformation6();
        mandateRelatedInformation6.setMndtId(mandate.getUmr());
        mandateRelatedInformation6.setDtOfSgntr(mandate.getDateOfSignatureAsGregorianCalendar());
        mandateRelatedInformation6.setFnlColltnDt(mandate.getCollectionDateAsGregorianCalendar());

        if (payment.isAmended()) {
            mandateRelatedInformation6.setAmdmntInd(Boolean.TRUE);
            AmendmentInformationDetails6 amendmentInformationDetails6 = buildAmendmentInformationDetails(payment);
            mandateRelatedInformation6.setAmdmntInfDtls(amendmentInformationDetails6);
        }

        directDebitTransaction.setMndtRltdInf(mandateRelatedInformation6);

        return directDebitTransaction;
    }

    private AmendmentInformationDetails6 buildAmendmentInformationDetails(DirectDebitPayment payment) {
        AmendmentDetails amendmentDetails = payment.getAmendmentDetails();
        PreviousDebitorAccount originalDebitorCreditorAccount = amendmentDetails.getOriginalDebitorAccount();
        OriginalCreditorScheme originalCreditorScheme = amendmentDetails.getOriginalCreditorScheme();

        AmendmentInformationDetails6 amendmentInformationDetails6 = new AmendmentInformationDetails6();

        amendmentInformationDetails6.setOrgnlMndtId(amendmentDetails.getOriginalMandateUmr());

        amendmentInformationDetails6.setOrgnlCdtrSchmeId(new PartyIdentification32());
        amendmentInformationDetails6.getOrgnlCdtrSchmeId().setNm(originalDebitorCreditorAccount.getName());
        amendmentInformationDetails6.getOrgnlCdtrSchmeId().setId(new Party6Choice());
        amendmentInformationDetails6.getOrgnlCdtrSchmeId().getId().setPrvtId(new PersonIdentification5());

        GenericPersonIdentification1 genericPersonIdentification1 = new GenericPersonIdentification1();
        genericPersonIdentification1.setId(originalCreditorScheme.getId().getBic());
        genericPersonIdentification1.setSchmeNm(new PersonIdentificationSchemeName1Choice());
        genericPersonIdentification1.getSchmeNm().setPrtry(originalCreditorScheme.getName());

        amendmentInformationDetails6.getOrgnlCdtrSchmeId().getId().getPrvtId().getOthr().add(genericPersonIdentification1);

        amendmentInformationDetails6.setOrgnlDbtrAcct(new CashAccount16());
        amendmentInformationDetails6.getOrgnlDbtrAcct().setId(new AccountIdentification4Choice());
        amendmentInformationDetails6.getOrgnlDbtrAcct().getId().setIBAN(originalDebitorCreditorAccount.getIban());

        return amendmentInformationDetails6;
    }

}
