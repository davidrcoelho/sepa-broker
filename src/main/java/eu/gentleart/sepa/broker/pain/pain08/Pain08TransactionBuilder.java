package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.controllers.exception.PainDocumentGenerationException;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPayment;
import iso.std.iso._20022.tech.xsd.pain_008_001.*;

import javax.xml.datatype.DatatypeConfigurationException;

class Pain08TransactionBuilder {

    private DirectDebitPayment payment;

    Pain08TransactionBuilder withPayment(DirectDebitPayment payment) {
        this.payment = payment;
        return this;
    }

    DirectDebitTransactionInformation9 build() {

        PaymentIdentification1 paymentIdentification1 = new Pain08PaymentIdentificationBuilder()
                .withPaymentIdentification(payment.getIdentification())
                .build();

        ActiveOrHistoricCurrencyAndAmount currencyAndAmount = new Pain08CurrencyAndAmountBuilder()
                .withInstructedAmount(payment.getInstructedAmount())
                .build();

        DirectDebitTransaction6 directDebitTransaction;
        try {
            directDebitTransaction = new Pain08MandateBuilder()
                    .withPayment(payment)
                    .build();
        } catch (DatatypeConfigurationException e) {
            throw new PainDocumentGenerationException(e);
        }

        BranchAndFinancialInstitutionIdentification4 institutionIdentification = new Pain08InstitutionIdentificationBuilder()
                .withBic(payment.getDebitorAccount().getBic())
                .build();

        PartyIdentification32 partyIdentification = new Pain08PartyIdentificationBuilder()
                .withName(payment.getDebitorAccount().getName())
                .build();

        CashAccount16 cashAccount = new Pain08CashAccountBuilder()
                .withIban(payment.getDebitorAccount().getIban())
                .build();

        RemittanceInformation5 remittanceInformation = new Pain08RemittanceInformationBuilder()
                .withRemittance(payment.getRemittance())
                .build();

        DirectDebitTransactionInformation9 directDebitTransactionInfo = new DirectDebitTransactionInformation9();
        directDebitTransactionInfo.setPmtId(paymentIdentification1);
        directDebitTransactionInfo.setInstdAmt(currencyAndAmount);
        directDebitTransactionInfo.setDrctDbtTx(directDebitTransaction);
        directDebitTransactionInfo.setDbtrAgt(institutionIdentification);
        directDebitTransactionInfo.setDbtr(partyIdentification);
        directDebitTransactionInfo.setDbtrAcct(cashAccount);
        directDebitTransactionInfo.setRmtInf(remittanceInformation);

        return directDebitTransactionInfo;
    }

}
