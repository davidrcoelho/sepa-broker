package eu.gentleart.sepa.broker.pain.pain08;

import iso.std.iso._20022.tech.xsd.pain_008_001.LocalInstrument2Choice;
import iso.std.iso._20022.tech.xsd.pain_008_001.PaymentTypeInformation20;
import iso.std.iso._20022.tech.xsd.pain_008_001.SequenceType1Code;
import iso.std.iso._20022.tech.xsd.pain_008_001.ServiceLevel8Choice;

class Pain08PaymentTypeBuilder {

    private static final String SCHEME_NAME = "SEPA";
    private static final String LOCAL_INSTRUMENT_CODE = "COR1";

    private String sequenceType;

    Pain08PaymentTypeBuilder withSequenceType(String sequenceType) {
        this.sequenceType = sequenceType;
        return this;
    }

    PaymentTypeInformation20 build() {
        PaymentTypeInformation20 paymentType = new PaymentTypeInformation20();
        paymentType.setSvcLvl(new ServiceLevel8Choice());
        paymentType.getSvcLvl().setCd(SCHEME_NAME);
        paymentType.setLclInstrm(new LocalInstrument2Choice());
        paymentType.getLclInstrm().setCd(LOCAL_INSTRUMENT_CODE);
        paymentType.setSeqTp(SequenceType1Code.fromValue(sequenceType));
        return paymentType;
    }

}
