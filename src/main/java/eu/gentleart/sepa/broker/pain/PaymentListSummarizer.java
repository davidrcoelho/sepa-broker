package eu.gentleart.sepa.broker.pain;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPaymentRepository;
import iso.std.iso._20022.tech.xsd.pain_008_001.SequenceType1Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PaymentListSummarizer {

    private static final Logger logger = LoggerFactory.getLogger(PaymentListSummarizer.class);

    private final DirectDebitPaymentRepository paymentRepository;
    private final DirectDebitDocument directDebitDocument;

    private Map<String, PaymentListSummary> paymentsBySequenceType = new HashMap<>();

    public PaymentListSummarizer(DirectDebitPaymentRepository paymentRepository, DirectDebitDocument directDebitDocument) {

        this.paymentRepository = paymentRepository;
        this.directDebitDocument = directDebitDocument;

        for (SequenceType1Code sequenceTypeCode : SequenceType1Code.values()) {
            String sequenceType = sequenceTypeCode.value();

            logger.info("Getting summary for sequenceType: {}", sequenceType);

            PaymentListSummary summary = summarizePaymentsBySequenceType(sequenceType);

            if (summary.hasPayments()) {
                paymentsBySequenceType.put(sequenceType, summary);
            }

        }

    }

    public int getTotalNumberOfTransactions() {
        return paymentRepository.getQuantityOfPayments(directDebitDocument);
    }

    public BigDecimal getTotalPaymentSum() {
        return paymentRepository.getSumOfPaymentsAmount(directDebitDocument);
    }

    private PaymentListSummary summarizePaymentsBySequenceType(String sequenceType) {

        BigDecimal sum = paymentRepository.getSumOfPaymentsAmountBySequenceType(directDebitDocument, sequenceType);
        logger.info("Total sum for sequenceType: {}, sum: {}", sequenceType, sum);

        int size = paymentRepository.getQuantityOfPaymentsAmountBySequenceType(directDebitDocument, sequenceType);
        logger.info("Total size for sequenceType: {}, size: {}", sequenceType, size);

        PageablePaymentsListWrapper pageablePaymentsListWrapper = new PageablePaymentsListWrapper(paymentRepository, directDebitDocument, sequenceType);

        return new PaymentListSummary(sequenceType, pageablePaymentsListWrapper, sum, size);
    }

    public Collection<PaymentListSummary> listSummary() {
        return paymentsBySequenceType.values();
    }
}

