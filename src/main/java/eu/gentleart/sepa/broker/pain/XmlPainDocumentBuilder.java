package eu.gentleart.sepa.broker.pain;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPaymentRepository;
import eu.gentleart.sepa.broker.pain.pain08.Pain08XmlDocumentBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.io.OutputStream;

@Component
public class XmlPainDocumentBuilder {

    private DirectDebitPaymentRepository paymentRepository;

    @Autowired
    public XmlPainDocumentBuilder(DirectDebitPaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public void build(DirectDebitDocument document, OutputStream outputStream) throws DatatypeConfigurationException, JAXBException, IOException {
        new Pain08XmlDocumentBuilder()
                .withPaymentRepository(paymentRepository)
                .withDirectDebitDocument(document)
                .build(outputStream);
    }

}
