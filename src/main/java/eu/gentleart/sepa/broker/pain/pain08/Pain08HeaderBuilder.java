package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.directdebit.document.Creditor;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DocumentHeader;
import iso.std.iso._20022.tech.xsd.pain_008_001.*;

import javax.xml.datatype.DatatypeConfigurationException;
import java.math.BigDecimal;

class Pain08HeaderBuilder {

    private DirectDebitDocument document;
    private int totalNumberOfTransactions;
    private BigDecimal totalPaymentSum;

    Pain08HeaderBuilder withDocument(DirectDebitDocument document) {
        this.document = document;
        return this;
    }

    Pain08HeaderBuilder withTotalNumberOfTransactions(int totalNumberOfTransactions) {
        this.totalNumberOfTransactions = totalNumberOfTransactions;
        return this;
    }

    Pain08HeaderBuilder withTotalPaymentSum(BigDecimal totalPaymentSum) {
        this.totalPaymentSum = totalPaymentSum;
        return this;
    }

    GroupHeader39 build() throws DatatypeConfigurationException {
        DocumentHeader header = document.getDocumentHeader();

        GroupHeader39 gropuHeader = new GroupHeader39();
        gropuHeader.setMsgId(header.getMessageId());
        gropuHeader.setCreDtTm(header.getCreditorDateAsGregorianCalendar());
        gropuHeader.setNbOfTxs(String.valueOf(totalNumberOfTransactions));
        gropuHeader.setCtrlSum(totalPaymentSum);
        gropuHeader.setInitgPty(getCreditorIdentification());
        return gropuHeader;
    }

    private PartyIdentification32 getCreditorIdentification() {

        Creditor creditor = document.getPaymentHeader().getCreditor();

        PartyIdentification32 creditorNamePartyIdendification = new Pain08PartyIdentificationBuilder()
            .withName(creditor.getName())
            .build();

        GenericOrganisationIdentification1 genericOrganisationIdentification1 = new GenericOrganisationIdentification1();
        genericOrganisationIdentification1.setId(creditor.getSchemeId());

        OrganisationIdentification4 organisationIdentification4 = new OrganisationIdentification4();
        organisationIdentification4.getOthr().add(genericOrganisationIdentification1);

        Party6Choice party6Choice = new Party6Choice();
        party6Choice.setOrgId(organisationIdentification4);
        creditorNamePartyIdendification.setId(party6Choice);

        return creditorNamePartyIdendification;
    }

}
