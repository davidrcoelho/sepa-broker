package eu.gentleart.sepa.broker.pain;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPayment;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPaymentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Objects;
import java.util.function.Consumer;

public class PageablePaymentsListWrapper {

    private static final Logger logger = LoggerFactory.getLogger(PageablePaymentsListWrapper.class);

    private final DirectDebitPaymentRepository repository;
    private final DirectDebitDocument document;
    private final String sequenceType;

    public PageablePaymentsListWrapper(DirectDebitPaymentRepository repository, DirectDebitDocument document, String sequenceType) {
        this.repository = repository;
        this.document = document;
        this.sequenceType = sequenceType;
    }

    public void forEach(Consumer<DirectDebitPayment> consumer) {
        Objects.requireNonNull(consumer);

        logger.info("Querying paged data");
        Page<DirectDebitPayment> page = repository.findByDocumentAndSequenceType(document, sequenceType, PageRequest.of(0, 100000));
        logger.info("Data loaded. page: {}", page);

        while (page.hasContent()) {

            page.forEach(consumer::accept);

            if (!page.hasNext()) break;

            logger.info("Querying paged data");
            page = repository.findByDocumentAndSequenceType(document, sequenceType, page.nextPageable());
            logger.info("Data loaded. page: {}", page);
        }

    }

}
