package eu.gentleart.sepa.broker.pain;

import iso.std.iso._20022.tech.xsd.pain_008_001.Document;
import iso.std.iso._20022.tech.xsd.pain_008_001.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.OutputStream;

public class XmlMarshaller {

    public void marshall(Document document, OutputStream outputStream) throws JAXBException, IOException {

        JAXBContext jaxbContext = JAXBContext.newInstance(Document.class);

        JAXBElement jaxbDoc = new ObjectFactory().createDocument(document);
        Marshaller marshaller = jaxbContext.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(jaxbDoc, outputStream);
        outputStream.flush();
    }

}
