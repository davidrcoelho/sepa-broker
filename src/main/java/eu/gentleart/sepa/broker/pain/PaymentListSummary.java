package eu.gentleart.sepa.broker.pain;

import java.math.BigDecimal;

public class PaymentListSummary {

    private final String sequenceType;
    private final PageablePaymentsListWrapper payments;
    private final BigDecimal sum;
    private final int size;

    public PaymentListSummary(String sequenceType, PageablePaymentsListWrapper pageablePaymentsListWrapper, BigDecimal sum, int size) {
        this.sequenceType = sequenceType;
        this.payments = pageablePaymentsListWrapper;
        this.sum = sum;
        this.size = size;
    }

    public String getSequenceType() {
        return sequenceType;
    }

    public PageablePaymentsListWrapper getPayments() {
        return payments;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public boolean hasPayments() {
        return size > 0;
    }

    public int size() {
        return size;
    }
}
