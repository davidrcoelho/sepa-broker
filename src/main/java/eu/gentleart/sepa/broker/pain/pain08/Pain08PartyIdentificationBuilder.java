package eu.gentleart.sepa.broker.pain.pain08;

import iso.std.iso._20022.tech.xsd.pain_008_001.PartyIdentification32;

class Pain08PartyIdentificationBuilder {

    private String name;

    Pain08PartyIdentificationBuilder withName(String name) {
        this.name = name;
        return this;
    }

    PartyIdentification32 build() {
        PartyIdentification32 partyIdentification = new PartyIdentification32();
        partyIdentification.setNm(name);
        return partyIdentification;
    }

}
