package eu.gentleart.sepa.broker.pain.storage;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public interface Pain08FileStorage {

    String getPain08Filename(DirectDebitDocument document);
    boolean pain08FileExists(DirectDebitDocument document);

    String getMD5Filename(String filename);

    FileOutputStream getFileOutputStream(String filename) throws FileNotFoundException;
    FileInputStream getFileInputStream(String filename) throws FileNotFoundException;
    FileInputStream createMD5File(String filename, String md5Filename) throws IOException;

    void delete(String filename);
    void removeOldFiles();

}
