package eu.gentleart.sepa.broker.pain.pain08;

import eu.gentleart.sepa.broker.directdebit.payment.Remittance;
import iso.std.iso._20022.tech.xsd.pain_008_001.RemittanceInformation5;

class Pain08RemittanceInformationBuilder {

    private Remittance remittance;

    Pain08RemittanceInformationBuilder withRemittance(Remittance remittance) {
        this.remittance = remittance;
        return this;
    }

    RemittanceInformation5 build() {
        RemittanceInformation5 remittanceInformation = new RemittanceInformation5();
        remittanceInformation.getUstrd().add(remittance.getUnstructured());
        return remittanceInformation;
    }
}
