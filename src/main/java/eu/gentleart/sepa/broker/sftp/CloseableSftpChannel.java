package eu.gentleart.sepa.broker.sftp;

import com.jcraft.jsch.*;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public class CloseableSftpChannel implements Closeable {

    private static final long ZERO_BYTE = 0;

    private ChannelSftp channelSftp;

    public CloseableSftpChannel(ChannelSftp channelSftp) {
        this.channelSftp = channelSftp;
    }

    @Override
    public void close() throws IOException {
        try {
            Session session = channelSftp.getSession();
            session.disconnect();
            channelSftp.disconnect();
        } catch (JSchException e) {
            throw new IOException(e);
        }
    }

    public InputStream get(String src) throws SftpException {
        return channelSftp.get(src);
    }

    public void put(InputStream inputStream, String filename) throws SftpException {
        channelSftp.put(inputStream, filename);
    }

    public boolean exists(String filename) throws SftpException {
        try {
            SftpATTRS lstat = channelSftp.lstat(filename);
            return lstat.getSize() > ZERO_BYTE;
        } catch (SftpException e) {
            if(e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                return false;
            } else {
                throw e;
            }
        }
    }

}
