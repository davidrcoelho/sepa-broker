package eu.gentleart.sepa.broker.sftp;

import com.jcraft.jsch.*;

import java.nio.charset.Charset;

public class SFTPHelper {

    private SFTPHelper() {}

    public static CloseableSftpChannel createChannel(String privateKey, String username, String password, String host, int port) throws JSchException {

        JSch jsch = new JSch();

        if(!privateKey.isEmpty()){
            byte[] privateKeyBytes = privateKey.getBytes(Charset.defaultCharset());
            jsch.addIdentity(
                    username,
                    privateKeyBytes,
                    null,
                    new byte[]{}
            );
        }

        Session session = jsch.getSession(username, host, port);
        session.setConfig("StrictHostKeyChecking", "no");
        session.setPassword(password);

        session.connect();
        ChannelSftp channel = createChannel(session);
        return new CloseableSftpChannel(channel);
    }

    private static ChannelSftp createChannel(Session session) throws JSchException {
        Channel channel = session.openChannel("sftp");
        channel.connect();
        return (ChannelSftp) channel;
    }
}
