package eu.gentleart.sepa.broker.sftp;


import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;


public class SftpConnector {

    private static final Logger logger = LoggerFactory.getLogger(SftpConnector.class);

    private final String privateKey;
    private final String username;
    private final String password;
    private final String host;
    private final int port;
    private final String remoteFolder;

    public static SftpConnector create(Map<String, Object> params) {
        String host = (String) params.get("host");
        Integer port = (Integer) params.get("port");
        String username = (String) params.get("username");
        String password = (String) params.get("password");
        String privateKey = (String) params.get("privateKey");
        String remoteFolder = (String) params.get("remoteFolder");
        return new SftpConnector(privateKey, username, password, host, port, remoteFolder);
    }

    public SftpConnector(String privateKey, String username, String password, String host, int port, String remoteFolder) {
        this.privateKey = privateKey;
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        this.remoteFolder = remoteFolder;
    }

    public void uploadIfDoesNotExists(String filename, FileInputStream file) throws JSchException, SftpException, IOException {
        if (!exists(filename)) {
            upload(filename, file);
        }
    }

    private void upload(String filename, InputStream fileContent) throws JSchException, IOException, SftpException {

        String remoteFilenamePath = getFilePath(filename);

        logger.info("Uploading filename: {}", remoteFilenamePath);

        try (CloseableSftpChannel channel = SFTPHelper.createChannel(privateKey, username, password, host, port)) {
            channel.put(fileContent, remoteFilenamePath);
        }

        logger.info("File sent to the server filename: {}", remoteFilenamePath);
    }

    private String getFilePath(String filename) {
        return remoteFolder + "/" + filename;
    }

    public String download(String filename) throws JSchException, SftpException, IOException {

        String filePath = getFilePath(filename);

        logger.info("Downloading file from the server. filename: {}", filePath);

        try (CloseableSftpChannel channel = SFTPHelper.createChannel(privateKey, username, password, host, port)) {
            InputStream inputStream = channel.get(filePath);
            return IOUtils.toString(inputStream, Charset.defaultCharset());
        }
    }

    public boolean exists(String filename) throws JSchException, IOException, SftpException {
        String filePath = getFilePath(filename);

        try (CloseableSftpChannel channel = SFTPHelper.createChannel(privateKey, username, password, host, port)) {
            return channel.exists(filePath);
        }
    }

}
