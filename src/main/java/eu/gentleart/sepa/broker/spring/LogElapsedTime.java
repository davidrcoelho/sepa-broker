package eu.gentleart.sepa.broker.spring;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface LogElapsedTime {

    long elapsedTimeTresholdInMillis() default 50;

}
