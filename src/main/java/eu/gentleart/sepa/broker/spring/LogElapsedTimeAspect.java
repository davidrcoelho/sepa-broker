package eu.gentleart.sepa.broker.spring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.lang.reflect.Method;

@Aspect
@Component
public class LogElapsedTimeAspect {

    private static final Logger logger = LoggerFactory.getLogger(LogElapsedTimeAspect.class);

    @Pointcut(value="execution(public * *(..))")
    public void anyPublicMethod() { }

    @Around("@annotation(LogElapsedTime)")
    public void profileExecuteMethod(ProceedingJoinPoint call) throws Throwable {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        MethodSignature signature = (MethodSignature) call.getSignature();
        String methodName = signature.toShortString();
        Method method = signature.getMethod();

        LogElapsedTime logElapsedTime = method.getAnnotation(LogElapsedTime.class);

        call.proceed();
        stopWatch.stop();

        long totalTimeMillis = stopWatch.getTotalTimeMillis();

        if (totalTimeMillis >= logElapsedTime.elapsedTimeTresholdInMillis()) {
            logger.info("{} took {} millis", methodName, stopWatch.getTotalTimeMillis());
        }

    }

}
