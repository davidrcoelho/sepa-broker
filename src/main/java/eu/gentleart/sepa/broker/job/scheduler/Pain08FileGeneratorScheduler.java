package eu.gentleart.sepa.broker.job.scheduler;

import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.JobStatus;
import eu.gentleart.sepa.broker.controllers.Pain08FileGeneratorController;
import eu.gentleart.sepa.broker.spring.LogElapsedTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class Pain08FileGeneratorScheduler {

    private static final Logger logger = LoggerFactory.getLogger(Pain08FileGeneratorScheduler.class);

    @Autowired
    private DirectDebitDocumentController documentController;

    @Autowired
    private Pain08FileGeneratorController fileGeneratorController;

    @LogElapsedTime
    @Scheduled(fixedDelayString = "${jobs.fixedDelayInMills.pain08Generator}")
    public void scheduled() {

        List<DirectDebitDocument> documents = documentController.listByJobScheduleDateAndJobStatus(new Date(), JobStatus.CREATED);
        logger.info("Document jobs to be executed. size: {}", documents.size());

        for (DirectDebitDocument document : documents) {

            try {
                fileGeneratorController.generateFile(document);
            } catch (Exception e) {
                logger.error("Error executing the job for the document: " + document, e);
            }

        }

    }

}
