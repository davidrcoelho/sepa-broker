package eu.gentleart.sepa.broker.job.sender;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;

public interface DocumentSenderExecutor {

    void send(DirectDebitDocument document);

}
