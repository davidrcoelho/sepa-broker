package eu.gentleart.sepa.broker.job;

import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DocumentLocker {

    public interface Consumer {
        void accept();
    }

    private static final Logger logger = LoggerFactory.getLogger(DocumentLocker.class);

    private final DirectDebitDocumentController documentController;

    @Autowired
    public DocumentLocker(DirectDebitDocumentController documentController) {
        this.documentController = documentController;
    }

    public void executeWithLock(Long documentId, Consumer consumer) {
        try {

            if (documentController.acquireLock(documentId)) {
                consumer.accept();
            } else {
                logger.warn("It was not possible to acquire lock to Document. Another process got lock to it. documentId: {}", documentId);
            }

        } finally {
            logger.info("Releasing lock for document documentId: {}", documentId);
            documentController.releaseLock(documentId);
        }

    }


}
