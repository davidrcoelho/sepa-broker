package eu.gentleart.sepa.broker.job;

public class DocumentProcessingException extends RuntimeException {

    public DocumentProcessingException(Throwable throwable) {
        super(throwable);
    }

    public DocumentProcessingException(String s) {
        super(s);
    }

}
