package eu.gentleart.sepa.broker.job.scheduler;

import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.controllers.JobNotifierController;
import eu.gentleart.sepa.broker.directdebit.document.JobStatus;
import eu.gentleart.sepa.broker.spring.LogElapsedTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class JobEventNotifierScheduler {

    private static final Logger logger = LoggerFactory.getLogger(JobEventNotifierScheduler.class);

    @Autowired
    private DirectDebitDocumentController controller;

    @Autowired
    private JobNotifierController jobNotifier;

    @LogElapsedTime
    @Scheduled(fixedDelayString = "${jobs.fixedDelayInMills.jobNotifier}")
    public void scheduled() {

        List<DirectDebitDocument> documentsToBeNotified = controller.listByJobScheduleDateAndJobStatus(new Date(), JobStatus.SENT);
        logger.info("Document jobs to be notified. size: {}", documentsToBeNotified.size());

        for (DirectDebitDocument document : documentsToBeNotified) {
            jobNotifier.notify(document);
        }

    }

}
