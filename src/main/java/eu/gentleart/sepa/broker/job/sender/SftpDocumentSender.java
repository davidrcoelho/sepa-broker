package eu.gentleart.sepa.broker.job.sender;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import eu.gentleart.sepa.broker.directdebit.document.Action;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitJob;
import eu.gentleart.sepa.broker.job.DocumentProcessingException;
import eu.gentleart.sepa.broker.pain.storage.Pain08FileStorage;
import eu.gentleart.sepa.broker.sftp.SftpConnector;
import eu.gentleart.sepa.broker.spring.LogElapsedTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;

@Component("sftp-file-sender")
public class SftpDocumentSender implements DocumentSenderExecutor {

    private static final Logger logger = LoggerFactory.getLogger(SftpDocumentSender.class);

    private final DirectDebitDocumentController documentController;
    private final Pain08FileStorage pain08FileStorage;

    @Autowired
    public SftpDocumentSender(DirectDebitDocumentController documentController, Pain08FileStorage pain08FileStorage) {
        this.documentController = documentController;
        this.pain08FileStorage = pain08FileStorage;
    }

    @LogElapsedTime
    public void send(DirectDebitDocument document) {

        DirectDebitJob job = document.getJob();

        if (job.wasSent()) {
            logger.info("Job already SENT by another process. document: {}", document);

        } else {

            if (pain08FileStorage.pain08FileExists(document)) {
                sendToSftpServer(document);
                job.markAsSent();

            } else {
                logger.info("File not found for documentId: {}. Status will be set to CREATED to be processed again by file generator job.", document.getId());
                job.markAsCreated();
            }

            documentController.merge(document);
        }

    }

    private void sendToSftpServer(DirectDebitDocument document) {

        DirectDebitJob job = document.getJob();
        Action action = job.getAction();

        logger.info("Uploading pain08 xml file: {}", document);

        try {
            String pain08XmlFilename = pain08FileStorage.getPain08Filename(document);
            String md5Filename = pain08FileStorage.getMD5Filename(pain08XmlFilename);

            FileInputStream pain08File = pain08FileStorage.getFileInputStream(pain08XmlFilename);
            FileInputStream md5File = pain08FileStorage.createMD5File(pain08XmlFilename, md5Filename);

            SftpConnector sftpConnector = SftpConnector.create(action.getParams());
            sftpConnector.uploadIfDoesNotExists(pain08XmlFilename, pain08File);
            sftpConnector.uploadIfDoesNotExists(md5Filename, md5File);

            pain08FileStorage.delete(pain08XmlFilename);
            pain08FileStorage.delete(md5Filename);
            pain08FileStorage.removeOldFiles();

        } catch (IOException | SftpException | JSchException e) {
            throw new DocumentProcessingException(e);
        }
    }

}
