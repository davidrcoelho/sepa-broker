package eu.gentleart.sepa.broker.job.scheduler;

import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import eu.gentleart.sepa.broker.controllers.Pain08SenderController;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.JobStatus;
import eu.gentleart.sepa.broker.spring.LogElapsedTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class Pain08SenderScheduler {

    private static final Logger logger = LoggerFactory.getLogger(Pain08SenderScheduler.class);

    @Autowired
    private DirectDebitDocumentController controller;

    @Autowired
    private Pain08SenderController pain08SenderController;

    @LogElapsedTime
    @Scheduled(fixedDelayString = "${jobs.fixedDelayInMills.pain08Sender}")
    public void scheduled() {

        List<DirectDebitDocument> documentsToBeSent = controller.listByJobScheduleDateAndJobStatus(new Date(), JobStatus.EXECUTED);
        logger.info("Document jobs to be sent. size: {}", documentsToBeSent.size());

        for (DirectDebitDocument document : documentsToBeSent) {
            pain08SenderController.send(document);
        }

    }

}
