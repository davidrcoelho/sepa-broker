package eu.gentleart.sepa.broker.job.sender;

import eu.gentleart.sepa.broker.job.DocumentProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DocumentSenderFinder {

    private final Map<String, DocumentSenderExecutor> jobs;

    @Autowired
    public DocumentSenderFinder(Map<String, DocumentSenderExecutor> jobs) {
        this.jobs = jobs;
    }

    public DocumentSenderExecutor find(String kind) {
        if (!jobs.containsKey(kind)) {
            throw new DocumentProcessingException("No JobExecutor found with kind: " + kind);
        }
        return jobs.get(kind);
    }

}
