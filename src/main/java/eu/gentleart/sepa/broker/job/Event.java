package eu.gentleart.sepa.broker.job;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Date;

@Getter
public class Event {

    private long documentId;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private OffsetDateTime datetime;

    private String type;

    public Event(long documentId, OffsetDateTime datetime, String type) {
        this.documentId = documentId;
        this.datetime = datetime;
        this.type = type;
    }
}
