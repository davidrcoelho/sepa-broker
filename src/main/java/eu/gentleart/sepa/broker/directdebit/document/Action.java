package eu.gentleart.sepa.broker.directdebit.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter @Setter @ToString
public class Action {

    @NotNull
    private String kind;

    @NotNull
    private Map<String, Object> params;

    @NotNull
    private List<String> callback;

    public List<String> getCallback() {
        if (callback == null) {
            callback = new ArrayList<>();
        }
        return callback;
    }

    public <T> T getParam(String key) {
        return (T) params.get(key);
    }

    public Map<String, Object> getParams() {
        return params;
    }

}
