package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Getter
@Valid
@Embeddable
public class OriginalCreditorScheme {

    @NotBlank
    @Column(name = "original_creditor_scheme_name")
    private String name;

    @Embedded
    private SchemeIdentification id;

}
