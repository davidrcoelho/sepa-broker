package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Getter
@Valid
public class InstructedAmount {

    @NotBlank
    @Column(name = "instructed_amount_currency")
    private String currency;

    @Column(name = "instructed_amount_value")
    private BigDecimal value;

}
