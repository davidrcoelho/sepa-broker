package eu.gentleart.sepa.broker.directdebit.document;

public interface DirectDebitDocumentRepositoryCustom {

    int acquireLock(long documentId);

    int releaseLock(long documentId);
}
