package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;

@Getter
@Valid
@Embeddable
public class InstitutionIdentification {

    @Embedded
    private Institution institution;

}
