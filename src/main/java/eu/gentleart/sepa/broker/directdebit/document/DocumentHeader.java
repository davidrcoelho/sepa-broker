package eu.gentleart.sepa.broker.directdebit.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.gentleart.sepa.broker.directdebit.DateConverter;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.ZonedDateTime;

@Getter
@Embeddable @ToString
public class DocumentHeader {

    @NotBlank
    @Column(name = "document_header_message_id")
    private String messageId;

    @NotNull
    @Column(name = "document_header_creditor_datetime")
    private ZonedDateTime creditorDateTime;

    @JsonIgnore
    public XMLGregorianCalendar getCreditorDateAsGregorianCalendar() throws DatatypeConfigurationException {
        return DateConverter.gregorianCalendar(creditorDateTime);
    }

}
