package eu.gentleart.sepa.broker.directdebit.document;

import eu.gentleart.sepa.broker.api.JpaActionConverterJson;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Embeddable
@Getter @Setter @ToString
public class DirectDebitJob {

    @Column(name = "job_status")
    @Enumerated(EnumType.STRING)
    private JobStatus status = JobStatus.CREATED;

    @NotNull
    @Column(name = "job_schedule_date")
    private Date scheduleDate;

    @Convert(converter = JpaActionConverterJson.class)
    private Action action;

    public void markAsExecuted() {
        status = JobStatus.EXECUTED;
    }

    public void markAsNotified() {
        status = JobStatus.NOTIFIED;
    }

    public void markAsSent() {
        status = JobStatus.SENT;
    }

    public void markAsCreated() {
        status = JobStatus.CREATED;
    }

    public boolean wasExecuted() {
        return JobStatus.EXECUTED.equals(status);
    }

    public boolean wasSent() {
        return JobStatus.SENT.equals(status);
    }
}

