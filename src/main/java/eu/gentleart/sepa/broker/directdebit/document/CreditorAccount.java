package eu.gentleart.sepa.broker.directdebit.document;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Embeddable
@Getter @ToString
public class CreditorAccount {

    @NotBlank
    @Column(name = "creditor_account_iban")
    private String iban;

    @NotBlank
    @Column(name = "creditor_account_bic")
    private String bic;
}
