package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Getter
@Embeddable
public class SchemeIdentification {

    @NotBlank
    @Column(name = "scheme_identification_original_bic")
    private String bic;

    @NotBlank
    @Column(name = "scheme_identification_original_name")
    private String name;
}
