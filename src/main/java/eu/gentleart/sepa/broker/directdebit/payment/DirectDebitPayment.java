package eu.gentleart.sepa.broker.directdebit.payment;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Entity
public class DirectDebitPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @Setter
    private String requestId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @Version
    private long version;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @Setter
    private DirectDebitDocument document;

    @NotBlank
    @Setter
    private String sequenceType;

    @Embedded @Valid
    @Setter
    private PaymentIdentification identification;

    @Embedded @Valid
    @Setter
    private InstructedAmount instructedAmount;

    @Embedded @Valid
    @Setter
    private InstitutionIdentification institutionIdentification;

    @Embedded @Valid
    @Setter
    private DebitorAccount debitorAccount;

    @Embedded @Valid
    @Setter
    private Remittance remittance;

    @Embedded @Valid
    @Setter
    private Mandate mandate;

    @Embedded @Valid
    private AmendmentDetails amendmentDetails;

    public void setAmendmentDetails(AmendmentDetails amendmentDetails) {
        if (amendmentDetails != null) {
            this.amendmentDetails = amendmentDetails;
        }
    }

    public boolean isAmended() {
        return amendmentDetails != null;
    }
}


