package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Valid
@Getter
public class Remittance {

    @NotBlank
    private String unstructured;

}
