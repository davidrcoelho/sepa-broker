package eu.gentleart.sepa.broker.directdebit.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Date;

@Entity
@Getter @ToString
public class DirectDebitDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Setter
    @Column(unique = true)
    private String requestId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @Version
    private long version;

    @Setter
    @Embedded
    private DocumentHeader documentHeader;

    @Setter
    @Embedded
    private PaymentHeader paymentHeader;

    @Setter
    @Embedded
    private DirectDebitJob job;

    @Column(name = "lock_datetime")
    private OffsetDateTime lockDatetime;

}

