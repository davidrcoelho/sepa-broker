package eu.gentleart.sepa.broker.directdebit.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Embeddable
@Getter @Setter @ToString
public class Creditor {

    @Column(name = "creditor_id")
    private String id;

    @Column(name = "creditor_scheme_id")
    private String schemeId;

    @NotBlank
    @Column(name = "creditor_name")
    private String name;

    @Valid
    @Embedded
    private CreditorAccount account;

}
