package eu.gentleart.sepa.broker.directdebit.document;

public enum JobStatus {
    CREATED,
    EXECUTED,
    SENT,
    NOTIFIED;
}
