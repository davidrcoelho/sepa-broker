package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Getter
@Valid
public class PaymentIdentification {

    @NotBlank
    @Column(name = "payment_identification_end_to_end_id")
    private String endToEndId;

}
