package eu.gentleart.sepa.broker.directdebit.document;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface DirectDebitDocumentRepository extends CrudRepository<DirectDebitDocument, Long>, DirectDebitDocumentRepositoryCustom {

    DirectDebitDocument getByRequestId(String requestId);

    List<DirectDebitDocument> findByJobScheduleDateLessThanEqualAndJobStatus(Date scheduleDate, JobStatus jobStatus);

}
