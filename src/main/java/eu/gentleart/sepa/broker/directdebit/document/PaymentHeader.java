package eu.gentleart.sepa.broker.directdebit.document;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Embeddable
@Getter @ToString
public class PaymentHeader {

    @NotBlank
    @Column(name = "payment_header_id")
    private String id;

    @NotBlank
    @Column(name = "payment_header_payment_method")
    private String paymentMethod;

    @NotNull
    @Column(name = "payment_header_requested_collection_date")
    private OffsetDateTime requestedCollectionDate;

    @Valid
    @Embedded
    private Creditor creditor;

}
