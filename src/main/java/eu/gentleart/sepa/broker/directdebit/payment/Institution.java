package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Getter
@Embeddable
public class Institution {

    @NotBlank
    @Column(name = "institution_bic")
    private String bic;
}
