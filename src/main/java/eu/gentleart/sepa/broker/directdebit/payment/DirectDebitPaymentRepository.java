package eu.gentleart.sepa.broker.directdebit.payment;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface DirectDebitPaymentRepository extends CrudRepository<DirectDebitPayment, Long> {

    DirectDebitPayment findByRequestId(String requestId);

//    @Query("SELECT p FROM DirectDebitPayment p where p.document = :document")
    Page<DirectDebitPayment> findByDocumentAndSequenceType(@Param("document") DirectDebitDocument document, @Param("sequenceType") String sequenceType, Pageable pageable);

    @Query("SELECT count(0) FROM DirectDebitPayment p where p.document = :document")
    int getQuantityOfPayments(@Param("document") DirectDebitDocument document);

    @Query("SELECT sum(p.instructedAmount.value) FROM DirectDebitPayment p where p.document = :document")
    BigDecimal getSumOfPaymentsAmount(@Param("document") DirectDebitDocument directDebitDocument);

    @Query("SELECT sum(p.instructedAmount.value) FROM DirectDebitPayment p where p.document = :document and p.sequenceType = :sequenceType")
    BigDecimal getSumOfPaymentsAmountBySequenceType(@Param("document") DirectDebitDocument directDebitDocument, @Param("sequenceType") String sequenceType);

    @Query("SELECT count(0) FROM DirectDebitPayment p where p.document = :document and p.sequenceType = :sequenceType")
    int getQuantityOfPaymentsAmountBySequenceType(@Param("document") DirectDebitDocument directDebitDocument, @Param("sequenceType") String sequenceTyp);

}
