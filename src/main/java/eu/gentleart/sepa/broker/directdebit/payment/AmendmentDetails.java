package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.constraints.NotBlank;

@Getter @Setter
@Embeddable
public class AmendmentDetails {

    @NotBlank
    @Column(name = "amendment_details_original_mandate_umr")
    private String originalMandateUmr;

    @Embedded
    private OriginalCreditorScheme originalCreditorScheme;

    @Embedded
    private PreviousDebitorAccount originalDebitorAccount;

}
