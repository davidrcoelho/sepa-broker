package eu.gentleart.sepa.broker.directdebit.payment;

import eu.gentleart.sepa.broker.directdebit.DateConverter;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.ZonedDateTime;

@Getter
@Valid
@Embeddable
public class Mandate {

    @NotBlank
    private String umr;

    @NotNull
    @Column(name = "mandate_date_of_signature")
    private ZonedDateTime dateOfSignature;

    @NotNull
    @Column(name = "mandate_collection_date")
    private ZonedDateTime collectionDate;

    public XMLGregorianCalendar getDateOfSignatureAsGregorianCalendar() throws DatatypeConfigurationException {
        return DateConverter.gregorianCalendar(dateOfSignature);
    }

    public XMLGregorianCalendar getCollectionDateAsGregorianCalendar() throws DatatypeConfigurationException {
        return DateConverter.gregorianCalendar(collectionDate);
    }

}
