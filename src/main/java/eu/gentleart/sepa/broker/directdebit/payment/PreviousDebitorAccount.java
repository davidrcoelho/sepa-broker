package eu.gentleart.sepa.broker.directdebit.payment;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Getter
@Embeddable
public class PreviousDebitorAccount {

    @NotBlank
    @Column(name = "previous_debitor_account_name")
    private String name;

    @NotBlank
    @Column(name = "previous_debitor_account_iban")
    private String iban;

    @NotBlank
    @Column(name = "previous_debitor_account_bic")
    private String bic;
}
