package eu.gentleart.sepa.broker.directdebit;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DateConverter {

    private DateConverter() {}

    public static XMLGregorianCalendar gregorianCalendar(ZonedDateTime date) throws DatatypeConfigurationException {
        Calendar cal = GregorianCalendar.from(date);

        XMLGregorianCalendar creationDate = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        creationDate.setYear(cal.get(Calendar.YEAR));
        creationDate.setMonth(cal.get(Calendar.MONTH) + 1);
        creationDate.setDay(cal.get(Calendar.DAY_OF_MONTH));
        creationDate.setHour(cal.get(Calendar.HOUR_OF_DAY));
        creationDate.setMinute(cal.get(Calendar.MINUTE));
        creationDate.setSecond(cal.get(Calendar.SECOND));
        return creationDate;
    }

}
