package eu.gentleart.sepa.broker.directdebit.document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Repository
public class DirectDebitDocumentRepositoryCustomImpl implements DirectDebitDocumentRepositoryCustom {

    private EntityManager entityManager;

    @Autowired
    public DirectDebitDocumentRepositoryCustomImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public int acquireLock(long documentId) {

        LocalDateTime twentyMinutesAgo = LocalDateTime.now().minus(20, ChronoUnit.MINUTES);

        Query lockQuery = entityManager.createQuery("UPDATE DirectDebitDocument SET lock_datetime = :lock_datetime where id = :id and (lock_datetime is null or lock_datetime <= :timeout_lock_datetime)");
        lockQuery.setParameter("id", documentId);
        lockQuery.setParameter("lock_datetime", LocalDateTime.now());
        lockQuery.setParameter("timeout_lock_datetime", twentyMinutesAgo);

        return lockQuery.executeUpdate();
    }

    @Override
    public int releaseLock(long documentId) {
        Query releaseLockQuery = entityManager.createQuery("UPDATE DirectDebitDocument SET lock_datetime = null where id = :id");
        releaseLockQuery.setParameter("id", documentId);
        return releaseLockQuery.executeUpdate();
    }

}
