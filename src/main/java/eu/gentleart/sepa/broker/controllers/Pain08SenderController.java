package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.directdebit.document.Action;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitJob;
import eu.gentleart.sepa.broker.job.DocumentLocker;
import eu.gentleart.sepa.broker.job.sender.DocumentSenderExecutor;
import eu.gentleart.sepa.broker.job.sender.DocumentSenderFinder;
import eu.gentleart.sepa.broker.spring.LogElapsedTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Controller
public class Pain08SenderController {

    private static final int TWO_MINUTES_IN_SECONDS = 2 * 60;

    private final DocumentLocker locker;
    private final DocumentSenderFinder senderFinder;

    @Autowired
    public Pain08SenderController(DocumentLocker locker, DocumentSenderFinder senderFinder) {
        this.locker = locker;
        this.senderFinder = senderFinder;
    }

    @LogElapsedTime
    @Transactional(propagation = Propagation.REQUIRES_NEW, timeout = TWO_MINUTES_IN_SECONDS)
    public void send(DirectDebitDocument document) {
        locker.executeWithLock(document.getId(), () -> executeSend(document));
    }

    private void executeSend(DirectDebitDocument document) {
        DirectDebitJob job = document.getJob();
        Action action = job.getAction();
        DocumentSenderExecutor documentSenderExecutor = senderFinder.find(action.getKind());
        documentSenderExecutor.send(document);
    }

}
