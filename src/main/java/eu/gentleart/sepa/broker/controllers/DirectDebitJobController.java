package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class DirectDebitJobController {

    private final DirectDebitDocumentController documentController;

    @Autowired
    public DirectDebitJobController(DirectDebitDocumentController directDebitDocumentController) {
        this.documentController = directDebitDocumentController;
    }

    public DirectDebitJob createJob(long documentId, DirectDebitJob job) {

        DirectDebitDocument document = documentController.findById(documentId);
        document.setJob(job);

        documentController.merge(document);

        return document.getJob();
    }

}
