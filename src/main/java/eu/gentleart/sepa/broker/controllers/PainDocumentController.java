package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.controllers.exception.PainDocumentGenerationException;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.pain.XmlPainDocumentBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.io.OutputStream;

@Controller
public class PainDocumentController {

    private Logger logger = LoggerFactory.getLogger(PainDocumentController.class);

    private final DirectDebitDocumentController directDebitDocumentController;
    private final XmlPainDocumentBuilder xmlPainDocumentBuilder;

    @Autowired
    public PainDocumentController(DirectDebitDocumentController directDebitDocumentController, XmlPainDocumentBuilder xmlPainDocumentBuilder) {
        this.directDebitDocumentController = directDebitDocumentController;
        this.xmlPainDocumentBuilder = xmlPainDocumentBuilder;
    }

    public void generateXmlPainDocument(long documentId, OutputStream outputStream) {
        DirectDebitDocument document = directDebitDocumentController.findById(documentId);

        try {
            xmlPainDocumentBuilder.build(document, outputStream);
        } catch (DatatypeConfigurationException | JAXBException | IOException e) {
            logger.error("Error on generating xml file for documentId: {}", documentId);
            throw new PainDocumentGenerationException(e);
        }
    }

}
