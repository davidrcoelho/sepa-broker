package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitJob;
import eu.gentleart.sepa.broker.job.DocumentLocker;
import eu.gentleart.sepa.broker.job.DocumentProcessingException;
import eu.gentleart.sepa.broker.pain.XmlPainDocumentBuilder;
import eu.gentleart.sepa.broker.pain.storage.Pain08FileStorage;
import eu.gentleart.sepa.broker.spring.LogElapsedTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileOutputStream;

@Controller
public class Pain08FileGeneratorController {

    private static final Logger logger = LoggerFactory.getLogger(Pain08FileGeneratorController.class);
    public static final int FIFTEEN_MINUTES = 15 * 60;

    private final DirectDebitDocumentController documentController;
    private final DocumentLocker documentLocker;
    private final XmlPainDocumentBuilder xmlDocumentBuilder;
    private final Pain08FileStorage pain08FileStorage;

    @Autowired
    public Pain08FileGeneratorController(DirectDebitDocumentController documentController, DocumentLocker documentLocker, XmlPainDocumentBuilder xmlDocumentBuilder, Pain08FileStorage pain08FileStorage) {
        this.documentController = documentController;
        this.documentLocker = documentLocker;
        this.xmlDocumentBuilder = xmlDocumentBuilder;
        this.pain08FileStorage = pain08FileStorage;
    }

    @LogElapsedTime
    @Transactional(propagation = Propagation.REQUIRES_NEW, timeout = FIFTEEN_MINUTES)
    public void generateFile(DirectDebitDocument document) {
        documentLocker.executeWithLock(document.getId(), () -> processFile(document));
    }

    private void processFile(DirectDebitDocument document) {
        DirectDebitJob job = document.getJob();

        if (job.wasExecuted()) {
            logger.info("Job already EXECUTED by other process. document: {}", document);

        } else {
            logger.info("Executing job for document: {}", document);

            generatePain08File(document);

            job.markAsExecuted();
            documentController.merge(document);

            logger.info("Job executed for document: {}", document);
        }

    }

    private void generatePain08File(DirectDebitDocument document) {
        try {

            logger.info("Generating pain08 xml file: {}", document);
            String filename = pain08FileStorage.getPain08Filename(document);

            try (FileOutputStream xmlOutputStream = pain08FileStorage.getFileOutputStream(filename)) {
                xmlDocumentBuilder.build(document, xmlOutputStream);
            }

        } catch (Exception e) {
            logger.error("Error processing PAIN 08 xml file, documentId: {}", document.getId());
            throw new DocumentProcessingException(e);

        }
    }

}
