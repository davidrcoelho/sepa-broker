package eu.gentleart.sepa.broker.controllers.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error on generating xml file")
public class PainDocumentGenerationException extends RuntimeException {

    public PainDocumentGenerationException(Exception e) {
        super(e);
    }

}

