package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.controllers.exception.DirectDebitDocumentConflictUpdateException;
import eu.gentleart.sepa.broker.controllers.exception.DirectDebitDocumentNotFound;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocumentRepository;
import eu.gentleart.sepa.broker.directdebit.document.JobStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class DirectDebitDocumentController {

    private static final Logger logger = LoggerFactory.getLogger(DirectDebitDocumentController.class);
    private static final int FIVE_SECONDS = 5;

    private DirectDebitDocumentRepository documentRepository;

    @Autowired
    public DirectDebitDocumentController(DirectDebitDocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    public DirectDebitDocument merge(DirectDebitDocument document) {

        logger.info("Merging document into database. document: {}", document);

        try {
            save(document);
            return document;
        } catch (DataIntegrityViolationException e) {
            logger.warn("Duplicated requests containing the same requestId: {}", document.getRequestId());
        }

        return documentRepository.getByRequestId(document.getRequestId());
    }

    protected void save(DirectDebitDocument document) {
        try {
            documentRepository.save(document);
        } catch (OptimisticLockingFailureException e) {
            logger.error("Cannot save direct debit document. Has it been modified meanwhile?. documentId: {} - {}", document.getId(), document);
            throw new DirectDebitDocumentConflictUpdateException(e);
        }
    }

    public List<DirectDebitDocument> listAll() {
        return (List<DirectDebitDocument>) documentRepository.findAll();
    }

    public DirectDebitDocument findById(long documentId) {
        Optional<DirectDebitDocument> paymentFile = documentRepository.findById(documentId);
        if (paymentFile.isPresent()) {
            return paymentFile.get();
        }

        throw new DirectDebitDocumentNotFound();
    }

    public DirectDebitDocument update(long documentId, DirectDebitDocument directDebitDocument) {
        DirectDebitDocument existingDirectDebitDocument = findById(documentId);

        if (hasSameRequestId(directDebitDocument, existingDirectDebitDocument)) {
            return existingDirectDebitDocument;
        }

        BeanUtils.copyProperties(directDebitDocument, existingDirectDebitDocument, "creationDate");
        save(existingDirectDebitDocument);
        return existingDirectDebitDocument;
    }

    private boolean hasSameRequestId(DirectDebitDocument first, DirectDebitDocument second) {
        return (first.getRequestId().equals(second.getRequestId()));
    }

    public void delete(Long documentId) {
        try {
            documentRepository.deleteById(documentId);
        } catch (EmptyResultDataAccessException e) {
            logger.error("Trying to delete a document that does not exists. message: {}", e.getMessage());
            throw new DirectDebitDocumentNotFound();
        }
    }

    public List<DirectDebitDocument> listByJobScheduleDateAndJobStatus(Date jobScheduleDate, JobStatus jobStatus) {
        return documentRepository.findByJobScheduleDateLessThanEqualAndJobStatus(jobScheduleDate, jobStatus);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, timeout = FIVE_SECONDS)
    public boolean acquireLock(long documentId) {
        int effectedRecords = documentRepository.acquireLock(documentId);
        return effectedRecords == 1;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void releaseLock(long documentId) {
        int effectedRecords = documentRepository.releaseLock(documentId);
        if (effectedRecords != 1) {
            logger.error("Error trying to release lock for documentId: {}, effectedRecords: {}", documentId, effectedRecords);
        }
    }

}
