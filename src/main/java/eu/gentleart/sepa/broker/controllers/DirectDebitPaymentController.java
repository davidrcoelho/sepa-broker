package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.controllers.exception.DirectDebitDocumentNotFound;
import eu.gentleart.sepa.broker.controllers.exception.DirectDebitPaymentNotFound;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPayment;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPaymentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import java.util.Optional;

@Controller
public class DirectDebitPaymentController {

    private static final Logger logger = LoggerFactory.getLogger(DirectDebitPaymentController.class);

    private final DirectDebitDocumentController directDebitDocumentController;
    private final DirectDebitPaymentRepository paymentRepository;

    @Autowired
    public DirectDebitPaymentController(DirectDebitDocumentController directDebitDocumentController, DirectDebitPaymentRepository paymentRepository) {
        this.directDebitDocumentController = directDebitDocumentController;
        this.paymentRepository = paymentRepository;
    }

    public DirectDebitPayment addPayment(String requestId, long documentId, DirectDebitPayment directDebitPayment) {

        DirectDebitDocument document = directDebitDocumentController.findById(documentId);

        directDebitPayment.setRequestId(requestId);
        directDebitPayment.setDocument(document);

        try {
            return paymentRepository.save(directDebitPayment);
        } catch (DataIntegrityViolationException e) {
            logger.warn("Duplicated requests containing the same requestId: {}", document.getRequestId());
        }

        return paymentRepository.findByRequestId(requestId);
    }

    public DirectDebitPayment findPaymentByDocumentIdAndPaymentId(Long documentId, Long paymentId) {
        Optional<DirectDebitPayment> directDebitPaymentOptional = paymentRepository.findById(paymentId);

        if (!directDebitPaymentOptional.isPresent()) {
            throw  new DirectDebitPaymentNotFound();
        }

        DirectDebitPayment directDebitPayment = directDebitPaymentOptional.get();

        if (!documentId.equals(directDebitPayment.getDocument().getId())) {
            throw new DirectDebitDocumentNotFound();
        }

        return directDebitPayment;
    }

    public void deletePayment(Long documentId, Long paymentId) {
        DirectDebitPayment payment = findPaymentByDocumentIdAndPaymentId(documentId, paymentId);
        paymentRepository.delete(payment);
    }


}
