package eu.gentleart.sepa.broker.controllers.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Direct Debit Payment not found")
public class DirectDebitPaymentNotFound extends RuntimeException {
}
