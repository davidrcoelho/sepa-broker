package eu.gentleart.sepa.broker.controllers.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Cannot save direct debit document. Has it been modified meanwhile?")
public class DirectDebitDocumentConflictUpdateException extends RuntimeException {
    public DirectDebitDocumentConflictUpdateException(Throwable e) {
        super(e);
    }
}
