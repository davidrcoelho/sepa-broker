package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.directdebit.document.Action;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitJob;
import eu.gentleart.sepa.broker.directdebit.document.JobStatus;
import eu.gentleart.sepa.broker.job.Event;
import eu.gentleart.sepa.broker.spring.LogElapsedTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.OffsetDateTime;
import java.util.List;

@Controller
public class JobNotifierController {

    private static final Logger logger = LoggerFactory.getLogger(JobNotifierController.class);

    private static final int FIVE_SECONDS = 5;

    private final DirectDebitDocumentController documentController;
    private final RestTemplate restTemplate;

    @Autowired
    public JobNotifierController(DirectDebitDocumentController documentController, RestTemplate restTemplate) {
        this.documentController = documentController;
        this.restTemplate = restTemplate;
    }

    @LogElapsedTime
    @Transactional(propagation = Propagation.REQUIRES_NEW, timeout = FIVE_SECONDS)
    public void notify(DirectDebitDocument document) {

        DirectDebitJob job = document.getJob();
        Action action = job.getAction();

        List<String> callbackList = action.getCallback();

        logger.info("Callback list to be notified: {}", callbackList);

        for (String callback : callbackList) {
            executeNotification(callback, document.getId(), job.getStatus());
        }

        job.markAsNotified();
        documentController.merge(document);
    }

    private void executeNotification(String callback, long documentId, JobStatus event) {
        logger.info("Notifying callback: {}, event: {}", callback, event);
        restTemplate.postForLocation(callback, new Event(documentId, OffsetDateTime.now(), event.toString()));
        logger.info("Notified callback: {}, event: {}", callback, event);
    }
}
