package eu.gentleart.sepa.broker.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gentleart.sepa.broker.directdebit.document.Action;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.io.Serializable;

public class JpaActionConverterJson implements AttributeConverter<Object, String>, Serializable {

    protected final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(Object meta) {
        try {
            return objectMapper.writeValueAsString(meta);
        } catch (JsonProcessingException e) {
            throw new JPAConverterJsonException(e);
        }
    }

    @Override
    public Action convertToEntityAttribute(String dbData) {
        try {
            return objectMapper.readValue(dbData, Action.class);
        } catch (IOException e) {
            throw new JPAConverterJsonException(e);
        }
    }
}
