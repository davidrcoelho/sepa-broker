package eu.gentleart.sepa.broker.api.resource;

import eu.gentleart.sepa.broker.api.DirectDebitJobRequest;
import eu.gentleart.sepa.broker.controllers.DirectDebitJobController;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPayment;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DirectDebitJobResource {

    private static final Logger logger = LoggerFactory.getLogger(DirectDebitJobResource.class);

    @Autowired
    private DirectDebitJobController jobController;

    @PostMapping(value = "/v1/direct-debit/documents/{id}/job", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectDebitPayment> createJob(
            @PathVariable("id") long documentId,
            @RequestBody DirectDebitJobRequest jobRequest) {

        logger.info("Creating Job for a document. documentId: {}", documentId);

        DirectDebitJob directDebitJob = jobController.createJob(documentId, jobRequest.getData());

        return new ResponseEntity(directDebitJob, HttpStatus.OK);
    }

}
