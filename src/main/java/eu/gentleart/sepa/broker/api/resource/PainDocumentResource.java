package eu.gentleart.sepa.broker.api.resource;

import eu.gentleart.sepa.broker.controllers.PainDocumentController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class PainDocumentResource {

    private Logger logger = LoggerFactory.getLogger(PainDocumentResource.class);

    @Autowired
    private PainDocumentController painDocumentController;

    @GetMapping(value = "/v1/direct-debit/documents/{id}/pain", produces= MediaType.APPLICATION_XML_VALUE)
    public void getDocument(@PathVariable("id") long documentId, HttpServletResponse response) throws IOException {

        logger.info("Returning xml pain document. Id: {}", documentId);
        painDocumentController.generateXmlPainDocument(documentId, response.getOutputStream());
    }

}
