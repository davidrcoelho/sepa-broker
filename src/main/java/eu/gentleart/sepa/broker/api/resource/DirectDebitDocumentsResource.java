package eu.gentleart.sepa.broker.api.resource;


import eu.gentleart.sepa.broker.api.DirectDebitDocumentRequest;
import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DirectDebitDocumentsResource {

    private final Logger logger = LoggerFactory.getLogger(DirectDebitDocumentsResource.class);

    static final String REQUEST_ID = "x-broker-request-id";

    @Autowired
    private DirectDebitDocumentController directDebitDocumentController;

    @GetMapping(value = "/v1/direct-debit/documents", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DirectDebitDocument>> getDocuments() {
        logger.info("Returning all payment files.");
        List<DirectDebitDocument> files = directDebitDocumentController.listAll();
        return new ResponseEntity(files, HttpStatus.OK);
    }

    @PostMapping(value = "/v1/direct-debit/documents", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectDebitDocument> createDocument(
            @RequestHeader(REQUEST_ID) String requestId,
            @RequestBody @Valid DirectDebitDocumentRequest request) {

        logger.info("Request Id: {}, data: {}", requestId, request);
        DirectDebitDocument file = directDebitDocumentController.merge(request.getData(requestId));
        return new ResponseEntity(file, HttpStatus.OK);
    }

    @GetMapping(value = "/v1/direct-debit/documents/{id}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectDebitDocument> getDocument(@PathVariable("id") long documentId) {
        logger.info("Returning document file. Id: {}", documentId);
        DirectDebitDocument document = directDebitDocumentController.findById(documentId);
        return new ResponseEntity(document, HttpStatus.OK);
    }

    @PutMapping(value = "/v1/direct-debit/documents/{id}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectDebitDocument> updateDocument(
            @RequestHeader(REQUEST_ID) String requestId,
            @PathVariable("id") long documentId,
            @RequestBody DirectDebitDocumentRequest request) {
        logger.info("Updating document file. Id: {}, Request: {}", documentId, request);
        DirectDebitDocument file = directDebitDocumentController.update(documentId, request.getData(requestId));
        return new ResponseEntity(file, HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/direct-debit/documents/{id}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteDocument(@PathVariable("id") Long documentId) {
        logger.info("Deleting document file. Id: {}", documentId);
        directDebitDocumentController.delete(documentId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
