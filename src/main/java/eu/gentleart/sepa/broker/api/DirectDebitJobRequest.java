package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.directdebit.document.Action;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitJob;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Setter
public class DirectDebitJobRequest {

    @NotNull
    private Date scheduleDate;

    private Action action;

    public DirectDebitJob getData() {
        DirectDebitJob job = new DirectDebitJob();
        job.setScheduleDate(scheduleDate);
        job.setAction(action);
        return job;
    }
}
