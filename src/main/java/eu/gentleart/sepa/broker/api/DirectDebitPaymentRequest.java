package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.directdebit.payment.*;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Setter
public class DirectDebitPaymentRequest {

    @NotBlank
    private String sequenceType;

    @Valid
    private PaymentIdentification identification;

    @Valid
    private InstructedAmount instructedAmount;

    @Valid
    private InstitutionIdentification institutionIdentification;

    @Valid
    private DebitorAccount debitorAccount;

    @Valid
    private Remittance remittance;

    @Valid
    private Mandate mandate;

    @Valid
    private AmendmentDetails amendmentDetails;

    public DirectDebitPayment getData(String requestId) {
        DirectDebitPayment payment = new DirectDebitPayment();

        payment.setRequestId(requestId);
        payment.setSequenceType(sequenceType);
        payment.setIdentification(identification);
        payment.setInstructedAmount(instructedAmount);
        payment.setInstitutionIdentification(institutionIdentification);
        payment.setDebitorAccount(debitorAccount);
        payment.setRemittance(remittance);
        payment.setMandate(mandate);
        payment.setAmendmentDetails(amendmentDetails);

        return payment;
    }

}


