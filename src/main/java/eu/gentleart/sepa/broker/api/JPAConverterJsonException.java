package eu.gentleart.sepa.broker.api;

public class JPAConverterJsonException extends RuntimeException {
    public JPAConverterJsonException(Throwable e) {
        super(e);
    }
}
