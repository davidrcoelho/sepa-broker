package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.directdebit.document.DocumentHeader;
import eu.gentleart.sepa.broker.directdebit.document.PaymentHeader;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;

@Setter @Getter @ToString
public class DirectDebitDocumentRequest {

    @Valid
    private DocumentHeader documentHeader;

    @Valid
    private PaymentHeader paymentHeader;

    public DirectDebitDocument getData(String requestId) {
        DirectDebitDocument directDebitDocument = new DirectDebitDocument();
        directDebitDocument.setRequestId(requestId);
        directDebitDocument.setDocumentHeader(documentHeader);
        directDebitDocument.setPaymentHeader(paymentHeader);
        return directDebitDocument;
    }

}
