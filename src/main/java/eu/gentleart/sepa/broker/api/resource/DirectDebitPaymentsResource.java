package eu.gentleart.sepa.broker.api.resource;

import eu.gentleart.sepa.broker.api.DirectDebitPaymentRequest;
import eu.gentleart.sepa.broker.controllers.DirectDebitPaymentController;
import eu.gentleart.sepa.broker.directdebit.payment.DirectDebitPayment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DirectDebitPaymentsResource {

    private Logger logger = LoggerFactory.getLogger(DirectDebitPaymentsResource.class);

    @Autowired
    private DirectDebitPaymentController directDebitDocumentController;

    @PostMapping(value = "/v1/direct-debit/documents/{id}/payments", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectDebitPayment> createPayment(
            @RequestHeader(DirectDebitDocumentsResource.REQUEST_ID) String requestId,
            @PathVariable("id") long documentId,
            @RequestBody DirectDebitPaymentRequest directDebitPayment) {

        logger.info("Request Id: {}, data: {}", requestId, directDebitPayment);

        DirectDebitPayment storedPayment = directDebitDocumentController.addPayment(requestId, documentId, directDebitPayment.getData(requestId));

        return new ResponseEntity(storedPayment, HttpStatus.OK);
    }

    @GetMapping(value = "/v1/direct-debit/documents/{id}/payments/{paymentId}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectDebitPayment> getPayment(
            @PathVariable("id") Long documentId,
            @PathVariable("paymentId") Long paymentId) {

        logger.info("Request documentId: {}, paymentId: {}", documentId, paymentId);

        DirectDebitPayment storedPayment = directDebitDocumentController.findPaymentByDocumentIdAndPaymentId(documentId, paymentId);

        return new ResponseEntity(storedPayment, HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/direct-debit/documents/{id}/payments/{paymentId}", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deletePayment(
            @PathVariable("id") long documentId,
            @PathVariable("paymentId") long paymentId) {

        logger.info("Request documentId: {}, paymentId: {}", documentId, paymentId);

        directDebitDocumentController.deletePayment(documentId, paymentId);

        return new ResponseEntity(HttpStatus.OK);
    }

}
