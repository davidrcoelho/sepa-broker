package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.controllers.exception.PainDocumentGenerationException;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.pain.XmlPainDocumentBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import java.io.IOException;
import java.io.OutputStream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class PainDocumentControllerTest {

    private DirectDebitDocumentController directDebitDocumentController;
    private XmlPainDocumentBuilder xmlPainDocumentBuilder;

    @BeforeEach
    public void beforeEach() {
        directDebitDocumentController = mock(DirectDebitDocumentController.class);

        xmlPainDocumentBuilder = mock(XmlPainDocumentBuilder.class);
    }

    @Test
    public void when_an_JAXBException_occurs_on_xml_generation_should_throw_PainDocumentGenerationException() throws JAXBException, DatatypeConfigurationException, IOException {

        DirectDebitDocument document = new DirectDebitDocument();
        when(directDebitDocumentController.findById(anyLong())).thenReturn(document);

        doThrow(JAXBException.class).when(xmlPainDocumentBuilder).build(eq(document), any());

        PainDocumentController painDocumentController = new PainDocumentController(directDebitDocumentController, xmlPainDocumentBuilder);

        assertThatThrownBy(() -> painDocumentController.generateXmlPainDocument(123456789, mock(OutputStream.class)))
                .isInstanceOf(PainDocumentGenerationException.class);

    }

    @Test
    public void when_an_DatatypeConfigurationException_occurs_on_xml_generation_should_throw_PainDocumentGenerationException() throws JAXBException, DatatypeConfigurationException, IOException {

        DirectDebitDocument document = new DirectDebitDocument();
        when(directDebitDocumentController.findById(anyLong())).thenReturn(document);
        doThrow(DatatypeConfigurationException.class).when(xmlPainDocumentBuilder).build(eq(document), any());

        PainDocumentController painDocumentController = new PainDocumentController(directDebitDocumentController, xmlPainDocumentBuilder);

        assertThatThrownBy(() -> painDocumentController.generateXmlPainDocument(123456789, any()))
                .isInstanceOf(PainDocumentGenerationException.class);

    }

}
