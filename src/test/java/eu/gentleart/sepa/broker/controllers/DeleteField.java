package eu.gentleart.sepa.broker.controllers;

import eu.gentleart.sepa.broker.JsonField;
import com.jayway.jsonpath.DocumentContext;

public class DeleteField extends JsonField {

    public DeleteField(String name) {
        super(name);
    }

    @Override
    public void changeValue(DocumentContext documentContext) {
        documentContext.delete(getName());
    }

}
