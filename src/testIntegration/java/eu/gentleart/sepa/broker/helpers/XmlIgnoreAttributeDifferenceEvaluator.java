package eu.gentleart.sepa.broker.helpers;

import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.DifferenceEvaluator;

public class XmlIgnoreAttributeDifferenceEvaluator implements DifferenceEvaluator {
    private String[] attributeNames;
    public XmlIgnoreAttributeDifferenceEvaluator(String... attributeNames) {
        this.attributeNames = attributeNames;
    }

    @Override
    public ComparisonResult evaluate(Comparison comparison, ComparisonResult outcome) {
        if (outcome == ComparisonResult.EQUAL)
            return outcome;
        final Node controlNode = comparison.getControlDetails().getTarget();

        for (String attributeName : attributeNames) {
            if (comparison.getControlDetails().getParentXPath().contains(attributeName)) {
                return ComparisonResult.SIMILAR;
            }

            if (controlNode instanceof Attr) {
                Attr attr = (Attr) controlNode;
                if (attr.getName().equals(attributeName)) {
                    return ComparisonResult.SIMILAR;
                }
            }
        }

        return outcome;
    }
}
