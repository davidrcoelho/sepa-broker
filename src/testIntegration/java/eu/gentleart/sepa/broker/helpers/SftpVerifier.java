package eu.gentleart.sepa.broker.helpers;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import eu.gentleart.sepa.broker.sftp.SftpConnector;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import java.io.IOException;
import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class SftpVerifier {

    private static final Logger logger = LoggerFactory.getLogger(SftpVerifier.class);

    private SftpConnector sftpConnector;

    public SftpVerifier(SftpConnector sftpConnector) {
        this.sftpConnector = sftpConnector;
    }

    public void waitUntilFileIsUploadedToSftpServer(Duration timeout, String filename, String expectedContentFile) {
        await()
                .pollInterval(Duration.ofSeconds(2))
                .atMost(timeout)
                .untilAsserted(() -> fileExistsAtSftpServer(filename, expectedContentFile));
    }

    public void fileExistsAtSftpServer(String filename, String expectedContentFile) throws JSchException, SftpException, IOException {

        if (!sftpConnector.exists(filename)) {
            logger.info("No file found yet. filename: {}", filename);
            Assertions.fail("No file found!");
        }

        logger.info("File found! Downloading it. filename: {}", filename);

        String actualFileContent = sftpConnector.download(filename);

        Diff diff = DiffBuilder.compare(expectedContentFile).withTest(actualFileContent)
                .checkForSimilar()
                .ignoreComments()
                .ignoreElementContentWhitespace()
                .ignoreWhitespace()
                .build();

        assertThat(diff.hasDifferences()).isFalse();

    }

}
