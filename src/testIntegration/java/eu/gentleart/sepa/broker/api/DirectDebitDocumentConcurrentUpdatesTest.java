package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.AbstractApiTest;
import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import eu.gentleart.sepa.broker.controllers.exception.DirectDebitDocumentConflictUpdateException;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class DirectDebitDocumentConcurrentUpdatesTest extends AbstractApiTest {

    @Autowired
    private DirectDebitDocumentController controller;

    @Test
    public void given_two_parallel_requests_to_update_the_same_document_should_perform_optimistic_lock() {

        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);

        DirectDebitDocument document1 = controller.findById(documentId);
        DirectDebitDocument document2 = controller.findById(documentId);

        System.out.print("Document1.version: " + document1.getVersion());
        System.out.print("Document2.version: " + document2.getVersion());

        document1.getPaymentHeader().getCreditor().setName("New name for document 1");
        document2.getPaymentHeader().getCreditor().setName("New name for document 2");

        controller.merge(document1);

        assertThatThrownBy(() -> controller.merge(document2))
            .isInstanceOf(DirectDebitDocumentConflictUpdateException.class);
    }

    @Test
    public void given_two_process_getting_lock_for_the_same_document_one_should_fail() {
        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);

        boolean firstLock = controller.acquireLock(documentId);
        boolean secondLock = controller.acquireLock(documentId);

        assertThat(firstLock).isTrue();
        assertThat(secondLock).isFalse();
    }

    @Test
    void given_a_process_lock_and_release_a_document_the_second_process_should_be_able_to_acquire_lock_to_the_same_document() {
        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);

        boolean firstLock = controller.acquireLock(documentId);
        controller.releaseLock(documentId);

        boolean secondLock = controller.acquireLock(documentId);

        assertThat(firstLock).isTrue();
        assertThat(secondLock).isTrue();
    }

    @Test
    void when_try_to_release_lock_to_a_document_that_does_not_exists_should_not_thrown_exception() {
        controller.releaseLock(123456);
    }

}
