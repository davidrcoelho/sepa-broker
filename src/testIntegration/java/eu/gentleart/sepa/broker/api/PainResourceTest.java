package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.AbstractApiTest;
import eu.gentleart.sepa.broker.LocalFileReader;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class PainResourceTest extends AbstractApiTest {

    private Logger logger = LoggerFactory.getLogger(PainResourceTest.class);

    private static String PAYMENT_BODY_WITH_NO_AMENDMENT;

    @BeforeAll
    protected static void beforeAll() {
        DocumentContext documentContext = JsonPath.parse(DEFAULT_PAYMENT_BODY);
        documentContext.delete("amendmentDetails");
        PAYMENT_BODY_WITH_NO_AMENDMENT = documentContext.jsonString();
    }

    @Test
    public void given_a_document_should_generate_pain_file() throws IOException {

        String expectedXmlResponse = LocalFileReader.readFile("pain/pain.008.001.02-amendment.xml");

        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);
        DirectDebitPaymentsResourceTest.createPayment(documentId, DEFAULT_PAYMENT_BODY);

        String actualXmlResponse =
            given()
                    .contentType(ContentType.JSON)
                    .get(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/pain", documentId)
            .then()
                    .log().all()
                    .statusCode(200)
                    .extract().asString();

        assertThat(verifyXmlAreEqual(expectedXmlResponse, actualXmlResponse)).isTrue();

    }

    @Test
    public void given_a_document_with_no_amendment_should_generate_pain_file() throws IOException {

        String expectedXmlResponse = LocalFileReader.readFile("pain/pain.008.001.02-no-amendment.xml");

        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);
        DirectDebitPaymentsResourceTest.createPayment(documentId, PAYMENT_BODY_WITH_NO_AMENDMENT);

        String actualXmlResponse =
                given()
                        .contentType(ContentType.JSON)
                        .get(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/pain", documentId)
                .then()
                        .log().all()
                        .statusCode(200)
                        .extract().asString();

        assertThat(verifyXmlAreEqual(expectedXmlResponse, actualXmlResponse)).isTrue();

    }

    private boolean verifyXmlAreEqual(String expected, String actual) {
        Diff diff = DiffBuilder.compare(expected).withTest(actual)
                //.withDifferenceEvaluator(new XmlIgnoreAttributeDifferenceEvaluator("CreDtTm", "ReqdColltnDt", "FnlColltnDt", "MsgId","PmtInfId"))
                .checkForSimilar()
                .ignoreComments()
                .ignoreElementContentWhitespace()
                .ignoreWhitespace()
                .build();

        if (diff.hasDifferences()) {
            logger.info("{}", diff.getDifferences());
        }

        return diff.hasDifferences() == false;
    }


}


