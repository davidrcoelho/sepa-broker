package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.AbstractApiTest;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static eu.gentleart.sepa.broker.helpers.UniqueNumberGenerator.newUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

public class DirectDebitDocumentsResourceTest extends AbstractApiTest {

    static final String HEADERS_REQUEST_ID = "x-broker-request-id";
    static final String DOCUMENTS_ROOT_PATH = "/v1/direct-debit/documents";

    private static String DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY;

    @BeforeAll
    public static void beforeAll() {
        DocumentContext documentContext = JsonPath.parse(DEFAULT_PAYMENT_DOCUMENT_BODY);
        documentContext.set("paymentHeader.creditor.name", "New Creditor Name");
        DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY = documentContext.jsonString();
    }

    @Test
    public void given_a_duplicated_request_should_return_the_same_payment_file_created_previously() {

        String requestId = newUUID();

        int documentIdOfFirstRequest = createDocument(requestId, DEFAULT_PAYMENT_DOCUMENT_BODY);
        int documentIdOfSecondRequest = createDocument(requestId, DEFAULT_PAYMENT_DOCUMENT_BODY);

        assertThat(documentIdOfFirstRequest).isEqualTo(documentIdOfSecondRequest);
    }

    @Test
    public void given_there_a_payment_file_should_return_list_of_payment_files() {

        createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);

        given()
                .contentType(ContentType.JSON)
                .get(DOCUMENTS_ROOT_PATH)
        .then()
                .log().all()
                .statusCode(200)
                .body("$.size()", greaterThan(0));
    }

    @Test
    public void given_there_is_a_payment_file_should_return_it_by_id() {
        int documentId = createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);

        given()
                .contentType(ContentType.JSON)
                .get(DOCUMENTS_ROOT_PATH + "/{id}", documentId)
        .then()
                .log().all()
                .statusCode(200)
                .body("paymentHeader.creditor.schemeId", equalTo("123456789000"));
    }

    @Test
    public void given_the_id_does_not_exists_should_return_404_error() {
        String nonExistingFileId = "01020304";

        given()
                .contentType(ContentType.JSON)
                .get(DOCUMENTS_ROOT_PATH + "/{id}", nonExistingFileId)
        .then()
                .log().all()
                .statusCode(404)
                .body("message", containsString("Direct debit document not found"));
    }

    @Test
    public void given_a_put_request_should_update_payment_document() {

        int documentId = createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);

        updatePaymentDocumentPUTRequest(newUUID(), documentId, DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY);
        updatePaymentDocumentPUTRequest(newUUID(), documentId, DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY);
        updatePaymentDocumentPUTRequest(newUUID(), documentId, DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY);

        given()
                .contentType(ContentType.JSON)
                .get(DOCUMENTS_ROOT_PATH + "/{id}", documentId)
        .then()
                .log().all()
                .statusCode(200)
                .body("version", equalTo(3))
                .body("paymentHeader.creditor.name", equalTo("New Creditor Name"));
    }

    @Test
    public void given_a_duplicated_put_request_then_the_second_request_should_be_ignored() {

        int documentId = createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);
        String requestId = newUUID();

        updatePaymentDocumentPUTRequest(requestId, documentId, DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY);
        updatePaymentDocumentPUTRequest(requestId, documentId, DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY);
        updatePaymentDocumentPUTRequest(requestId, documentId, DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY);

        given()
                .contentType(ContentType.JSON)
                .get(DOCUMENTS_ROOT_PATH + "/{id}", documentId)
        .then()
                .log().all()
                .statusCode(200)
                .body("version", equalTo(1));
    }

    @Test
    public void given_a_put_request_for_an_invalid_fileId_should_return_404_error() {

        String nonExistingFileId = "01020304";

        given()
                .contentType(ContentType.JSON)
                .header(HEADERS_REQUEST_ID, newUUID())
                .body(DEFAULT_UPDATE_PAYMENT_DOCUMENT_BODY)
                .put(DOCUMENTS_ROOT_PATH + "/{id}", nonExistingFileId)
        .then()
                .log().all()
                .statusCode(404)
                .body("message", containsString("Direct debit document not found"));
    }

    @Test
    public void given_a_delete_request_should_delete_a_specific_payment_file() {
        String nonExistingFileId = "01020304";

        given()
                .contentType(ContentType.JSON)
                .delete(DOCUMENTS_ROOT_PATH + "/{id}", nonExistingFileId)
        .then()
                .log().all()
                .statusCode(404)
                .body("message", containsString("Direct debit document not found"));
    }

    @Test
    public void given_a_delete_request_to_a_non_existing_file_should_return_404_error() {
        int documentId = createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);

        given()
                .contentType(ContentType.JSON)
                .delete(DOCUMENTS_ROOT_PATH + "/{id}", documentId)
        .then()
                .log().all()
                .statusCode(200);

        given()
                .contentType(ContentType.JSON)
                .get(DOCUMENTS_ROOT_PATH + "/{id}", documentId)
        .then()
                .log().all()
                .statusCode(404)
                .body("message", containsString("Direct debit document not found"));
    }

    public static int createDocument(String requestId, String body) {
        return
                given()
                        .contentType(ContentType.JSON)
                        .header(HEADERS_REQUEST_ID, requestId)
                        .body(body)
                        .post(DOCUMENTS_ROOT_PATH)
                .then()
                        .log().all()
                        .statusCode(200)
                        .body("$", hasKey("id"))
                        .extract().path("id");
    }

    static int createDocument(String body) {
        String requestId = newUUID();
        return createDocument(requestId, body);
    }

    static int updatePaymentDocumentPUTRequest(String requestId, int documentId, String body) {
        return
            given()
                    .contentType(ContentType.JSON)
                    .header(HEADERS_REQUEST_ID, requestId)
                    .body(body)
                    .put(DOCUMENTS_ROOT_PATH + "/{id}", documentId)
            .then()
                    .log().all()
                    .statusCode(200)
                    .body(".", hasKey("id"))
                    .extract().path("id");

    }

}
