package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.AbstractApiTest;
import eu.gentleart.sepa.broker.ChangeField;
import eu.gentleart.sepa.broker.helpers.OpenApiParser;
import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static eu.gentleart.sepa.broker.api.DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH;
import static eu.gentleart.sepa.broker.api.DirectDebitJobsResourceTest.createJob;
import static eu.gentleart.sepa.broker.api.DirectDebitPaymentsResourceTest.HEADERS_REQUEST_ID;
import static eu.gentleart.sepa.broker.helpers.UniqueNumberGenerator.newUUID;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.hasKey;

public class Pain08BigDocumentVerification {

    private static final long QUANTITY = 100000;

    public static void main(String[] args) throws InterruptedException {
        Pain08BigDocumentVerification app = new Pain08BigDocumentVerification();

        int documentId = app.createDocument();
        app.generateBigDocument(documentId);
        app.createJobForDocument(documentId);
    }

    int createDocument() {
        return
            RestAssured.given()
                    .contentType(ContentType.JSON)
                    .header(HEADERS_REQUEST_ID, newUUID())
                    .body(AbstractApiTest.DEFAULT_PAYMENT_DOCUMENT_BODY)
                    .post(DOCUMENTS_ROOT_PATH)
            .then()
                    .statusCode(200)
                    .body("$", hasKey("id"))
                    .extract().path("id");
    }

    public void generateBigDocument(int documentId) throws InterruptedException {

        RestAssured.config = RestAssuredConfig.config().logConfig(LogConfig.logConfig().enableLoggingOfRequestAndResponseIfValidationFails());

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

        for (int i = 0; i < QUANTITY; i++) {
            executor.submit(() -> {
                RestAssured.given()
                        .contentType(ContentType.JSON)
                        .header(HEADERS_REQUEST_ID, newUUID())
                        .body(AbstractApiTest.DEFAULT_PAYMENT_BODY)
                        .post(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments", documentId)
                .then()
                        .statusCode(200);
            });

        }

        executor.shutdown();
        while (!executor.awaitTermination(30, TimeUnit.SECONDS)) {
            System.out.println("Awaiting completion of threads. " + LocalDateTime.now());
        }

    }

    public void createJobForDocument(int documentId) {

        String jobRequestBody = OpenApiParser.exampleOf("DirectDebitJobRequest",
                new ChangeField("scheduleDate", now()),
                new ChangeField("action.params.port", 2222),
                new ChangeField("action.params.host", "127.0.0.1"),
                new ChangeField("action.params.privateKey", ""),
                new ChangeField("action.params.username", "sftp_user"),
                new ChangeField("action.params.password", "pass"),
                new ChangeField("action.params.remoteFolder", "/test"),
                new ChangeField("action.callback", new ArrayList<>())
        );
        createJob(documentId, jobRequestBody);

    }

    private static String now() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        return format.format(new Date());
    }

}
