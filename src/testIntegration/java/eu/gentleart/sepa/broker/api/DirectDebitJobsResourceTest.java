package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.AbstractApiTest;
import eu.gentleart.sepa.broker.ChangeField;
import eu.gentleart.sepa.broker.LocalFileReader;
import eu.gentleart.sepa.broker.helpers.HttpMockEndpoint;
import eu.gentleart.sepa.broker.helpers.HttpMockServer;
import eu.gentleart.sepa.broker.helpers.OpenApiParser;
import eu.gentleart.sepa.broker.helpers.SftpVerifier;
import eu.gentleart.sepa.broker.sftp.SftpConnector;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockserver.matchers.Times;
import org.springframework.http.HttpStatus;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import static eu.gentleart.sepa.broker.api.DirectDebitDocumentsResourceTest.createDocument;
import static eu.gentleart.sepa.broker.api.DirectDebitPaymentsResourceTest.createPayment;
import static org.awaitility.Awaitility.await;
import static org.mockserver.verify.VerificationTimes.exactly;

@Testcontainers
public class DirectDebitJobsResourceTest extends AbstractApiTest {

    private static final String SFTP_HOST = "127.0.0.1";
    private static final String SFTP_KEY = "";
    private static final String SFTP_USERNAME = "sftp_user";
    private static final String SFTP_PASSWORD = "pass";
    private static final String SFTP_REMOTE_FOLDER = "/test";

    private static final String SFTP_CONTAINER_COMMAND = SFTP_USERNAME + ":" + SFTP_PASSWORD + ":1001:1001:" + SFTP_REMOTE_FOLDER;

    private static final String CALLBACK_PATH = "/job/listener-callback-test";
    private static final String JOB_CALLBACK_EXPECTED_BODY = "json/direct_debit_document_jobs_callback.json";

    @Container
    public static GenericContainer sftpServer = new GenericContainer<>("atmoz/sftp")
            .withCommand(SFTP_CONTAINER_COMMAND)
            .withExposedPorts(22);

    private static SftpVerifier sftpVerifier;
    private static HttpMockServer httpMockServer;

    @BeforeAll
    public static void beforeAll() throws IOException {
        SftpConnector sftpConnector = new SftpConnector(SFTP_KEY, SFTP_USERNAME, SFTP_PASSWORD, SFTP_HOST, sftpServer.getMappedPort(22), SFTP_REMOTE_FOLDER);
        sftpVerifier = new SftpVerifier(sftpConnector);

        httpMockServer = new HttpMockServer()
                .withEndpoint(new HttpMockEndpoint()
                        .withMethod("POST")
                        .withPath(CALLBACK_PATH)
                        .withTimes(Times.exactly(1))
                        .withResponseStatusCode(HttpStatus.OK)
                        .withResponseDelayInSeconds(1)
                ).start();
    }

    private String now() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        return format.format(new Date());
    }

    @Test
    public void given_a_job_schedule_to_now_should_execute_it_and_send_the_pain08_xml_to_sftp_server() throws Exception {

        String jobRequestBody = OpenApiParser.exampleOf("DirectDebitJobRequest",
                new ChangeField("scheduleDate", now()),
                new ChangeField("action.params.port", sftpServer.getMappedPort(22)),
                new ChangeField("action.params.host", SFTP_HOST),
                new ChangeField("action.params.privateKey", SFTP_KEY),
                new ChangeField("action.params.username", SFTP_USERNAME),
                new ChangeField("action.params.password", SFTP_PASSWORD),
                new ChangeField("action.params.remoteFolder", SFTP_REMOTE_FOLDER),
                new ChangeField("action.callback[0]", httpMockServer.getCallbackUrl())
        );

        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);
        createPayment(documentId, DEFAULT_PAYMENT_BODY);
        createJob(documentId, jobRequestBody);

        String expectedXmlPain08Content = LocalFileReader.readFile("pain/pain.008.001.02-amendment.xml");

        String filename = "sepa-pain08_" + documentId + ".xml";
        sftpVerifier.waitUntilFileIsUploadedToSftpServer(Duration.ofSeconds(20), filename, expectedXmlPain08Content);

        String expectedCallbackBodyJson = LocalFileReader.readJsonFile(JOB_CALLBACK_EXPECTED_BODY,
                new ChangeField("documentId", documentId));

        await()
                .pollDelay(Duration.ofSeconds(1))
                .pollInterval(Duration.ofSeconds(2))
                .atMost(Duration.ofSeconds(20))
                .untilAsserted(() -> httpMockServer.verifyBodyRequest(exactly(1), expectedCallbackBodyJson, "datetime"));
    }

    public static void createJob(int documentId, String jobRequestBody) {
        given()
                .contentType(ContentType.JSON)
                .body(jobRequestBody)
                .post(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/job", documentId)
        .then()
                .log().all()
                .statusCode(200)
                .extract().path("id");
    }

}
