package eu.gentleart.sepa.broker.api;

import eu.gentleart.sepa.broker.AbstractApiTest;
import eu.gentleart.sepa.broker.helpers.UniqueNumberGenerator;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static eu.gentleart.sepa.broker.api.DirectDebitDocumentsResourceTest.createDocument;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DirectDebitPaymentsResourceTest extends AbstractApiTest {

    static final String HEADERS_REQUEST_ID = "x-broker-request-id";

    @Test
    public void given_post_request_should_store_a_new_payment_in_a_file() {

        int documentId = DirectDebitDocumentsResourceTest.createDocument(UniqueNumberGenerator.newUUID(), DEFAULT_PAYMENT_DOCUMENT_BODY);

        String requestId = UniqueNumberGenerator.newUUID();
        createPayment(requestId, documentId, DEFAULT_PAYMENT_BODY);
    }

    @Test
    public void given_duplicated_request_id_should_return_success() {

        int documentId = DirectDebitDocumentsResourceTest.createDocument(UniqueNumberGenerator.newUUID(), DEFAULT_PAYMENT_DOCUMENT_BODY);

        String requestId = UniqueNumberGenerator.newUUID();
        int firstPaymentId = createPayment(requestId, documentId, DEFAULT_PAYMENT_BODY);
        int secondPaymentId = createPayment(requestId, documentId, DEFAULT_PAYMENT_BODY);

        assertThat(firstPaymentId).isEqualTo(secondPaymentId);

        given()
                .contentType(ContentType.JSON)
                .get(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments/{paymentId}", documentId, firstPaymentId)
        .then()
                .log().all()
                .statusCode(200)
                .body("document.id", equalTo(documentId));
    }

    @Test
    public void should_add_multiple_payments_to_a_document() {

        int documentId = DirectDebitDocumentsResourceTest.createDocument(UniqueNumberGenerator.newUUID(), DEFAULT_PAYMENT_DOCUMENT_BODY);

        int firstPaymentId = createPayment(UniqueNumberGenerator.newUUID(), documentId, DEFAULT_PAYMENT_BODY);
        int secondPaymentId = createPayment(UniqueNumberGenerator.newUUID(), documentId, DEFAULT_PAYMENT_BODY);

        given()
                .contentType(ContentType.JSON)
                .get(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments/{paymentId}", documentId, firstPaymentId)
        .then()
                .log().all()
                .statusCode(200);

        given()
                .contentType(ContentType.JSON)
                .get(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments/{paymentId}", documentId, secondPaymentId)
        .then()
                .log().all()
                .statusCode(200);
    }

    @Test
    public void given_get_request_should_return_payment_information() {
        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);
        int paymentId = createPayment(documentId, DEFAULT_PAYMENT_BODY);
        given()
                .contentType(ContentType.JSON)
                .get(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments/{paymentId}", documentId, paymentId)
        .then()
                .log().all()
                .statusCode(200)
                .body("id", equalTo(paymentId));
    }

    @Test
    public void given_delete_request_should_remove_payment_information() {
        int documentId = DirectDebitDocumentsResourceTest.createDocument(DEFAULT_PAYMENT_DOCUMENT_BODY);
        int paymentId = createPayment(documentId, DEFAULT_PAYMENT_BODY);

        given()
                .contentType(ContentType.JSON)
                .delete(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments/{paymentId}", documentId, paymentId)
        .then()
                .log().all()
                .statusCode(200);

        given()
                .contentType(ContentType.JSON)
                .get(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments/{paymentId}", documentId, paymentId)
        .then()
                .log().all()
                .statusCode(404);
    }

    public static int createPayment(String requestId, int documentId, String directDebitPaymentBody) {
        return
            given()
                    .contentType(ContentType.JSON)
                    .header(HEADERS_REQUEST_ID, requestId)
                    .body(directDebitPaymentBody)
                    .post(DirectDebitDocumentsResourceTest.DOCUMENTS_ROOT_PATH + "/{id}/payments", documentId)
            .then()
                    .log().all()
                    .statusCode(200)
                    .extract().path("id");
    }

    public static int createPayment(int documentId, String directDebitPaymentBody) {
        String requestId = UniqueNumberGenerator.newUUID();
        return createPayment(requestId, documentId, directDebitPaymentBody);
    }

}
