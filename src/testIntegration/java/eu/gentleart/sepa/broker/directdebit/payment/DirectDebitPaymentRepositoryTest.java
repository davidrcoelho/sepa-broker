package eu.gentleart.sepa.broker.directdebit.payment;

import eu.gentleart.sepa.broker.AbstractApiTest;
import eu.gentleart.sepa.broker.api.DirectDebitDocumentsResourceTest;
import eu.gentleart.sepa.broker.controllers.DirectDebitDocumentController;
import eu.gentleart.sepa.broker.directdebit.document.DirectDebitDocument;
import eu.gentleart.sepa.broker.helpers.UniqueNumberGenerator;
import eu.gentleart.sepa.broker.pain.PageablePaymentsListWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static eu.gentleart.sepa.broker.api.DirectDebitPaymentsResourceTest.createPayment;

public class DirectDebitPaymentRepositoryTest extends AbstractApiTest {

    @Autowired
    private DirectDebitDocumentController controller;

    @Autowired
    private DirectDebitPaymentRepository repository;

    @Test
    void given_a_list_of_payments_should_wrap_it_on_a_interable() {
        int documentId = DirectDebitDocumentsResourceTest.createDocument(UniqueNumberGenerator.newUUID(), DEFAULT_PAYMENT_DOCUMENT_BODY);

        for (int i = 0; i < 20; i++) {
            createPayment(UniqueNumberGenerator.newUUID(), documentId, DEFAULT_PAYMENT_BODY);
        }

        DirectDebitDocument document = controller.findById(documentId);

        PageablePaymentsListWrapper wrapper = new PageablePaymentsListWrapper(repository, document, "FRST");
        wrapper.forEach((payment) -> {
            System.out.println(payment.getId() + "-" + payment.getAmendmentDetails().getOriginalMandateUmr());
//            System.out.println(payment.getId() + "-" + payment.getAmendmentDetails().getId() + "-" + payment.getAmendmentDetails().getOriginalDebitorAccount());
        });


    }
}



