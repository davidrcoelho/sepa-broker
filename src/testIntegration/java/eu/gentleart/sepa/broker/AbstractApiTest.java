package eu.gentleart.sepa.broker;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import eu.gentleart.sepa.broker.helpers.OpenApiParser;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
@ContextConfiguration(initializers = {AbstractApiTest.PropertiesInitializer.class})
public abstract class AbstractApiTest {

    public static final String DEFAULT_PAYMENT_DOCUMENT_BODY = OpenApiParser.exampleOf("DirectDebitDocumentRequest");
    public static final String DEFAULT_PAYMENT_BODY = OpenApiParser.exampleOf("DirectDebitPaymentRequest");

    protected static PostgreSQLContainer postgres = new PostgreSQLContainer<>("postgres:10.11")
            .withUsername("postgres")
            .withPassword("test")
            .withDatabaseName("postgres")
            .withExposedPorts(5432);

    static {
        postgres.start();
    }

    @LocalServerPort
    private int port;

    @BeforeEach
    public void beforeEach() {
        RestAssured.port = port;
    }

    public static RequestSpecification given() {
        return RestAssured.given()
                .log().all()
                ;//.filter(new OpenApiValidationFilter("apidoc.yaml"));
    }

    public static class PropertiesInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(configurableApplicationContext,
                    "spring.datasource.url=jdbc:postgresql://127.0.0.1:" + postgres.getMappedPort(5432) + "/postgres"
            );
        }
    }

}
