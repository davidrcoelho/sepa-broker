#!/bin/bash

set -x

COMMIT=$1

kubectl create configmap sepa-broker-apidoc --from-file=./src/testIntegration/resources/apidoc.yaml --dry-run=client -o yaml | kubectl apply -f -

helm upgrade --install sepa-broker https://drcoelho.github.io/sepa-broker-charts/sepa-broker-0.1-$COMMIT.tgz \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=drcoelho/sepa-broker:$COMMIT
